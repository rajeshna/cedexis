<?php
/*
*	Template name: Events
*/
?>
<?php get_header(); ?>

<?php if( have_posts() ) : while( have_posts() ) : the_post();  ?>

	<section id="masthead">
		<div class="row">
			<div class="content">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</div>
	</section>

<?php endwhile; ?>
<?php endif; ?>

	<section id="press-and-news">
		<div class="row">

<?php

	/*
	*
	*	GET THE YEAR (TAXONOMY)
	*	Construct left hand menu containing all years.
	*
	*/
	$year_args = array( 'hide_empty' => false, 'order' => 'DESC' );
	$years = get_terms( 'event_year', $year_args );
	$available_years = array();

	echo '<div class="sidebar"><ul class="years">';
			
			foreach ($years as $year) {
				
				echo '<li><a href="#" data-nonce="' . wp_create_nonce("load_events") . '">' . $year->name . '</a></li>';
				$available_years[] = $year->slug;

			}

	echo '</ul></div>';

	/*
	*
	*	GET EVENTS
	*	
	*/
	$current_year = date('Y');
	
/*
	$args = array(
		'post_type' => 'events',
		'posts_per_page' => -1,
		'tax_query' => array(
			array(
				'taxonomy' => 'event_year',
				'field'    => 'name',
				'terms'    => $current_year,
			),
		),
		'orderby'   => 'meta_value_num',
		'meta_type' => 'DATE',
		'meta_key'  => '_cedexis_event_date',
		'order'	=> 'DESC',
	);
*/
	
	
	$args = array(
		'post_type' => 'events',
		'posts_per_page' => -1,
		'tax_query' => array(
			array(
				'taxonomy' => 'event_year',
				'field'    => 'name',
				'terms'    => $current_year,
			),
		)
	);
		
	
	$query = new WP_Query($args);
	$press = $query->get_posts();
	
	function fix_events(&$events, $current_year){
	    foreach($events as &$event) {
	        if( false === strpos($event->_cedexis_event_date, '/') ) {
	            //die("ok");
	            $str = $event->_cedexis_event_date . " " . $current_year;
	            //die($str);
	            $date = DateTime::createFromFormat('M d Y',$str);
	            $output = $date->format('m/d/y');
	            //die($output);
	            $event->_cedexis_event_date = $output;
	            
	        } else {
	            $event->_cedexis_event_date = date('m/d/y', strtotime($event->_cedexis_event_date));
	        }
	    }
	   
	}
	
	fix_events($press, $current_year);
	
	usort($press, function($a, $b)
	{
	    return date($a->_cedexis_event_date) < date($b->_cedexis_event_date);
	});
?>

	<?php //$events = new WP_Query( $args ); ?>
	<?php //$i = 0; ?>
	<?php //if( $events->have_posts() ) : ?>
	
	
	<?php if( !empty($press) ) { ?>
	<div class="news">

	<?php
		$date_headings =array();
		foreach($press as $post) { 
		//while( $events->have_posts() ) : $events->the_post();

		//Post Meta Items
		$event_date = get_post_meta( $post->ID, '_cedexis_event_date', true ); 
		$event_url = get_post_meta( $post->ID, '_cedexis_event_url', true ); 
		$event_location = get_post_meta( $post->ID, '_cedexis_event_location', true ); 
		$event_details = get_post_meta( $post->ID, '_cedexis_event_details', true ); 

		//Conversions
		$date_convert = strtotime( $event_date );
		$date_heading = date('F Y', $date_convert);
		$date_small = date('M j', $date_convert );
			
	?>

			<?php 

				if( !in_array($date_heading, $date_headings ) ) { 

					$date_headings[] = $date_heading;
			?>
				</ul>
				<h3><?php echo $date_heading; ?></h3>
				<ul class="press-calendar events-calendar">
					
			<?php } ?>

				<li>
					<p><span class="event-dates"><strong class="event-start-date"><?php echo $date_small; ?></strong></span><a href="<?php echo $event_url; ?>"><?php the_title(); ?></a><em><?php echo $event_location; ?></em><span class="status"><?php echo $event_details; ?></span></p>
				</li>
				

<?php //$i++; ?>
<?php //endwhile; ?>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
</section>

<?php //wp_reset_postdata(); ?>
<?php //endif; ?>
<?php get_footer(); ?>
