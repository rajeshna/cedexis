<div id="home6" class="avia-section main_color avia-section-no-padding avia-no-border-styling avia-bg-style-scroll  avia-builder-el-40  el_after_av_section  avia-builder-el-last  container_wrap fullsize">
	<div class="container">
		<div class="template-page content  av-content-full alpha units">
			<div class="post-entry post-entry-type-page post-entry-5730">
				<div class="entry-content-wrapper clearfix">
					<div class="flex_column av_one_third  flex_column_div av-zero-column-padding first  avia-builder-el-41  el_before_av_one_third  avia-builder-el-first  " style="border-radius:0px; ">
						<section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
							<div class="avia_textblock " itemprop="text">
								<p style="text-align: left;">
									<img class="alignnone size-full wp-image-3379" src="http://s3-eu-west-1.amazonaws.com/cdx-website-wp-media/wp-content/uploads/20160513152820/32-social.png" alt="32-social" width="32" height="32">
								</p>
								<h6 class="news-title">FROM THE BLOG</h6>
								<?php
								$blog_query = new WP_Query( array( 'posts_per_page' => 1 ));
								if ( $blog_query->have_posts() ) { 
									while ( $blog_query->have_posts() ) { 
										$blog_query->the_post();
								?>
								<h5 style="text-align: left; padding-left: 42px;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
								<div style="text-align: left; padding-left: 42px;"><?php the_excerpt(); ?></div>
								<?php
									}
								}
								wp_reset_postdata();
								?>
							</div>
						</section>
					</div>
					<div class="flex_column av_one_third  flex_column_div av-zero-column-padding   avia-builder-el-43  el_after_av_one_third  el_before_av_one_third  " style="border-radius:0px; ">
						<section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
							<div class="avia_textblock " itemprop="text">
								<p style="text-align: left;">
									<img class="alignnone size-full wp-image-3380" src="http://s3-eu-west-1.amazonaws.com/cdx-website-wp-media/wp-content/uploads/20160513152820/32-news.png" alt="32-news" width="31" height="32">
								</p>
								<h6 class="news-title">IN THE NEWS</h6>
								<?php
								$new_query = new WP_Query( array( 'posts_per_page' => 3, 'post_type' => 'news', 'order' => 'DESC' ));
								if ( $new_query->have_posts() ) { 
									while ( $new_query->have_posts() ) { 
										$new_query->the_post();
										$press_url = get_post_meta( get_the_ID(), '_cedexis_press_url', true ); 
								?>
								<h5 style="text-align: left; padding-left: 42px; margin-bottom: 1em;"><a href="<?php echo $press_url; ?>">"<?php the_title(); ?>"<br></a></h5>
								<?php
									}
								}
								wp_reset_postdata();
								?>
							</div>
						</section>
					</div>
					<div class="flex_column av_one_third  flex_column_div av-zero-column-padding   avia-builder-el-45  el_after_av_one_third  avia-builder-el-last  " style="border-radius:0px; ">
						<section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
							<div class="avia_textblock " itemprop="text">
								<p style="text-align: left;">
									<img class="alignnone size-full wp-image-3380" src="http://s3-eu-west-1.amazonaws.com/cdx-website-wp-media/wp-content/uploads/20160513152821/32-events.png" alt="32-events" width="31" height="32">
								</p>
								<h6 class="news-title">UPCOMING EVENTS</h6>
								<?php
								$current_year = date('Y');
								$next_year = date('Y', strtotime("+1 Year"));

								$event_query = new WP_Query( array( 
									'posts_per_page' => 3, 
									'post_type' 	=> 'events',
									'tax_query' => array(
										array(
											'taxonomy' => 'event_year',
											'field'    => 'name',
											'terms'    => array($current_year, $next_year),
										),
									),
									'orderby'   => 'meta_value_num',
									'meta_type' => 'DATE',
									'meta_key'  => '_cedexis_event_date',
									'order'	=> 'DESC',
								));
								if ( $event_query->have_posts() ) { 
									while ( $event_query->have_posts() ) { 
										$event_query->the_post();
										$event_date = get_post_meta( get_the_ID(), '_cedexis_event_date', true ); 
										$event_url = get_post_meta( get_the_ID(), '_cedexis_event_url', true );
										$event_location = get_post_meta( get_the_ID(), '_cedexis_event_location', true ); 
										$date_convert = strtotime( $event_date );
										$date_small = date('M j', $date_convert );
								?>

								<h5 class="events"><a href="<?php echo $event_url; ?>"><?php the_title(); ?></a><small><?php echo $date_small; ?> in <?php echo $event_location; ?></small></h5>
								
								<?php
									}
								}
								wp_reset_postdata();
								?>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div><!-- close content main div --> <!-- section close by builder template -->		
	</div><!--end builder template-->
</div>