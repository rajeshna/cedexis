<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'THEME_DIR' ) && ! defined( 'THEME_URL' ) ) {

  define( 'THEME_DIR', get_stylesheet_directory() );
  define( 'THEME_URL', get_stylesheet_directory_uri() );

}
require_once( THEME_DIR . '/admin/ajax.php' );

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:


if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        // wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css' );
        wp_enqueue_style( 'custom_css', get_stylesheet_directory_uri() . '/custom_style.css' );
        wp_enqueue_style( 'temp', get_stylesheet_directory_uri() . '/temp.css', array(), '', 'all' );

    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css' );

// END ENQUEUE PARENT ACTION
add_action ('__after_header' , 'add_content_after_header', 20);

function add_content_after_header() {

	}

/*****************************************************
*
*	ADD OPTIONS FOR EVENTS & PRESS RELEASES
*
*****************************************************/
require_once( THEME_DIR . '/admin/options.php' );

/*****************************************************
*
*	EVENTS POST TYPE
*
*****************************************************/
if ( ! function_exists('cedexis_events_post_type') ) {

// Register Custom Post Type
function cedexis_events_post_type() {

	$labels = array(
		'name'                  => __( 'Events', 'Post Type General Name', 'cedexis' ),
		'singular_name'         => __( 'Event', 'Post Type Singular Name', 'cedexis' ),
		'menu_name'             => __( 'Events', 'cedexis' ),
		'name_admin_bar'        => __( 'New Event', 'cedexis' ),
		'archives'              => __( 'Item Archives', 'cedexis' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cedexis' ),
		'all_items'             => __( 'All Items', 'cedexis' ),
		'add_new_item'          => __( 'Add New Item', 'cedexis' ),
		'add_new'               => __( 'Add New', 'cedexis' ),
		'new_item'              => __( 'New Event', 'cedexis' ),
		'edit_item'             => __( 'Edit Event', 'cedexis' ),
		'update_item'           => __( 'Update Event', 'cedexis' ),
		'view_item'             => __( 'View Events', 'cedexis' ),
		'search_items'          => __( 'Search Events', 'cedexis' ),
		'not_found'             => __( 'Not found', 'cedexis' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cedexis' ),
		'featured_image'        => __( 'Featured Image', 'cedexis' ),
		'set_featured_image'    => __( 'Set featured image', 'cedexis' ),
		'remove_featured_image' => __( 'Remove featured image', 'cedexis' ),
		'use_featured_image'    => __( 'Use as featured image', 'cedexis' ),
		'insert_into_item'      => __( 'Insert into item', 'cedexis' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cedexis' ),
		'items_list'            => __( 'Items list', 'cedexis' ),
		'items_list_navigation' => __( 'Items list navigation', 'cedexis' ),
		'filter_items_list'     => __( 'Filter items list', 'cedexis' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'cedexis' ),
		'description'           => __( 'Cedexis Events', 'cedexis' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-location-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'events', $args );

}
add_action( 'init', 'cedexis_events_post_type', 0 );

}

/*****************************************************
*
*	PRESS POST TYPE
*
*****************************************************/
if ( ! function_exists('cedexis_press_post_type') ) {

// Register Custom Post Type
function cedexis_press_post_type() {

	$labels = array(
		'name'                  => __( 'News & Press', 'Post Type General Name', 'cedexis' ),
		'singular_name'         => __( 'News', 'Post Type Singular Name', 'cedexis' ),
		'menu_name'             => __( 'News & Press', 'cedexis' ),
		'name_admin_bar'        => __( 'New News', 'cedexis' ),
		'archives'              => __( 'Item Archives', 'cedexis' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cedexis' ),
		'all_items'             => __( 'All Items', 'cedexis' ),
		'add_new_item'          => __( 'Add New Item', 'cedexis' ),
		'add_new'               => __( 'Add New', 'cedexis' ),
		'new_item'              => __( 'New News', 'cedexis' ),
		'edit_item'             => __( 'Edit News', 'cedexis' ),
		'update_item'           => __( 'Update News', 'cedexis' ),
		'view_item'             => __( 'View Events', 'cedexis' ),
		'search_items'          => __( 'Search Events', 'cedexis' ),
		'not_found'             => __( 'Not found', 'cedexis' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cedexis' ),
		'featured_image'        => __( 'Featured Image', 'cedexis' ),
		'set_featured_image'    => __( 'Set featured image', 'cedexis' ),
		'remove_featured_image' => __( 'Remove featured image', 'cedexis' ),
		'use_featured_image'    => __( 'Use as featured image', 'cedexis' ),
		'insert_into_item'      => __( 'Insert into item', 'cedexis' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cedexis' ),
		'items_list'            => __( 'Items list', 'cedexis' ),
		'items_list_navigation' => __( 'Items list navigation', 'cedexis' ),
		'filter_items_list'     => __( 'Filter items list', 'cedexis' ),
	);
	$args = array(
		'label'                 => __( 'News & Press', 'cedexis' ),
		'description'           => __( 'Cedexis News & Press', 'cedexis' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-megaphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'news', $args );

}
add_action( 'init', 'cedexis_press_post_type', 0 );

}


/*****************************************************
*
*	ADD EVENT YEAR TAXONOMY
*
*****************************************************/
// Register Custom Taxonomy
function cedexis_event_year() {

	$labels = array(
		'name'                       => _x( 'Years', 'Taxonomy General Name', 'cedexis' ),
		'singular_name'              => _x( 'Year', 'Taxonomy Singular Name', 'cedexis' ),
		'menu_name'                  => __( 'Event Years', 'cedexis' ),
		'all_items'                  => __( 'All Items', 'cedexis' ),
		'parent_item'                => __( 'Parent Item', 'cedexis' ),
		'parent_item_colon'          => __( 'Parent Item:', 'cedexis' ),
		'new_item_name'              => __( 'New Item Name', 'cedexis' ),
		'add_new_item'               => __( 'Add New Item', 'cedexis' ),
		'edit_item'                  => __( 'Edit Item', 'cedexis' ),
		'update_item'                => __( 'Update Item', 'cedexis' ),
		'view_item'                  => __( 'View Item', 'cedexis' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'cedexis' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'cedexis' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'cedexis' ),
		'popular_items'              => __( 'Popular Items', 'cedexis' ),
		'search_items'               => __( 'Search Items', 'cedexis' ),
		'not_found'                  => __( 'Not Found', 'cedexis' ),
		'no_terms'                   => __( 'No items', 'cedexis' ),
		'items_list'                 => __( 'Items list', 'cedexis' ),
		'items_list_navigation'      => __( 'Items list navigation', 'cedexis' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'event_year', array( 'events' ), $args );

}
add_action( 'init', 'cedexis_event_year', 0 );

/*****************************************************
*
*	ADD PRESS YEAR TAXONOMY
*
*****************************************************/
// Register Custom Taxonomy
function cedexis_press_year() {

	$labels = array(
		'name'                       => _x( 'Years', 'Taxonomy General Name', 'cedexis' ),
		'singular_name'              => _x( 'Year', 'Taxonomy Singular Name', 'cedexis' ),
		'menu_name'                  => __( 'Press Years', 'cedexis' ),
		'all_items'                  => __( 'All Items', 'cedexis' ),
		'parent_item'                => __( 'Parent Item', 'cedexis' ),
		'parent_item_colon'          => __( 'Parent Item:', 'cedexis' ),
		'new_item_name'              => __( 'New Item Name', 'cedexis' ),
		'add_new_item'               => __( 'Add New Item', 'cedexis' ),
		'edit_item'                  => __( 'Edit Item', 'cedexis' ),
		'update_item'                => __( 'Update Item', 'cedexis' ),
		'view_item'                  => __( 'View Item', 'cedexis' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'cedexis' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'cedexis' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'cedexis' ),
		'popular_items'              => __( 'Popular Items', 'cedexis' ),
		'search_items'               => __( 'Search Items', 'cedexis' ),
		'not_found'                  => __( 'Not Found', 'cedexis' ),
		'no_terms'                   => __( 'No items', 'cedexis' ),
		'items_list'                 => __( 'Items list', 'cedexis' ),
		'items_list_navigation'      => __( 'Items list navigation', 'cedexis' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'press_year', array( 'news' ), $args );

}
add_action( 'init', 'cedexis_press_year', 0 );

/*****************************************************
*
*	Add body classes
*
*****************************************************/
function add_slug_body_class( $classes ) {

	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	if( is_single() ) {
		$classes[] = 'blog';
	}
	if( is_page_template( 'template-events.php')  ) {
		$classes[] = 'teal events';
	}

	if( is_page_template( 'template-press.php') ) {
		$classes[] = 'teal press';
	}

	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


function modify_read_more_link() {
return '<a class="more-link" href="' . get_permalink() . '">Read more...</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );
//SHOW TEMPLATE NAME IN HEAD
// add_action('wp_head', 'show_template');
function show_template() {
    global $template;
    print_r($template);
}

function cedexis_press_widgets_init() {
    register_sidebar( array(

        'name' => __( 'Press Release Page Sidebar', 'cedexis' ),
        'id' => 'press-sidebar',
        'description' => __( 'Widgets in this area will be shown on the press release page on the right hand side.', 'cedexis' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',

    ) );
}
add_action( 'widgets_init', 'cedexis_press_widgets_init' );

//	Add extra area above footer
function cedexis_extra_footer() {
	if(!is_page('create-account')) {
		include('extra-footer.php');
	}
}
add_action( 'ava_before_footer', 'cedexis_extra_footer' );

//	Shorten Excerpt Length
function cedexis_custom_excerpt_length( $length ) {

	if( is_home() ) {
		return 75;
	} else {
	    return 20;
	}
}
add_filter( 'excerpt_length', 'cedexis_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return sprintf( '<a class="more-link" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( 'Read more...', 'textdomain' )
    );
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );



/*****************************************************
*
*	Add signup form
*
*****************************************************/
function info( $msg ) {
	error_log( $msg );
}
function infoDump( $var ) {
	ob_start();
	var_dump($var);
	$result = ob_get_clean();
	info($result);
}

// disable SSL certificate verification for localhost (useful for testing)
// add_action( 'plugins_loaded', 'backwpup_disable_local_ssl_verify', 11 );
// function backwpup_disable_local_ssl_verify() {
// 	add_filter( 'https_local_ssl_verify', '__return_false' );
// }


/**
 * Custom Cedexis Portal Sign Up function.  Registers GravityForms callback
 * function to tie into a custom form and do an HTTP POST to the Cedexis
 * Portal API for account sign up.
 *
 * Full documentation:
 *
 * https://docs.google.com/a/cedexis.com/document/d/11dZU-orQE0_gvUme9Knagb-2E18xL6Coj_U0993uZBs/edit?usp=sharing
 *
 * @authon Jon Chase <jon.chase@gmail.com>
 */
add_filter('gform_validation_1', 'signup_validation'); // _1 is the id of the Gravity Form form in WordPress
function signup_validation($validation_result) {

	// dump the contents of the form - comment this out unless you are debugging
	// infoDump($validation_result);

	// the css classes of fields we're interested in are prefixed with this string
	$signup = 'signup-';


	//
	// do not attempt further validation is there is already a validation error
	//

	if (!$validation_result['is_valid']) {
		info('form is invalid, not attempting further validation');
		return $validation_result;
	}



	//
	// form is valid, so call the Portal API to try to do a sign up
	//


	// get the GravityForms form object from the validation result
    $form = $validation_result['form'];

    // loop through the form fields and collect them into a hashmap
    $params = array();
    foreach($form['fields'] as &$field) {
    	// infoDump($field);
        if (substr($field->cssClass, 0, 7) !== $signup) {
        	// if the field's css class doesn't start with 'signup-', skip it
        	info('skipping: ' . $field->cssClass);
        } else {
        	// this is a field we are interested in - save the param name and value
        	info('processing: ' . $field->cssClass);
        	$paramName = substr($field->cssClass, strlen($signup), strlen($field->cssClass));

        	// special case for newsletter checkbox
        	if ($paramName === 'newsletter') {
        		$checkboxId = 'input_' . str_replace('.', '_', $field->inputs[0]['id']);
        		// have to get value from POST, b/c not available via Gravity Form
        		$newsletter = $_POST[$checkboxId];

        		if (empty($newsletter)) {
        			$newsletter = FALSE;
        			info('newsletter = false');
        		} else {
        			$newsletter = TRUE;
        			info('newsletter = true');
        		}
        		infoDump($_POST);
        		$paramValue = $newsletter;

        	} else {
	        	$paramValue = rgpost("input_{$field['id']}");
	        	info('param val is ' . $paramValue);

	        	// special case for contact type
	        	if ($paramName === 'contactType') {
	        		if ($paramValue === 'business') {
	        			$paramName = 'businessContact';
	        		} else if ($paramValue == 'billing') {
						$paramName = 'billingContact';
	        		} else { // technical
						$paramName = 'technicalContact';
	        		}
	        		$paramValue = true;
	        	}
        	}

        	$params[$paramName] = $paramValue;
        }
    }

    // special case to get the Marketo cookie
	$marketoCookieName = '_mkto_trk';
	$marketoCookieValue = NULL;
	if(isset($_COOKIE[$marketoCookieName])) {
		$marketoCookieValue = $_COOKIE[$marketoCookieName];
	}


    info('preparing to post params:');
    infoDump($params);



    //
    // post the params to the portal API
    //


    // production url
    $postUrl = 'https://api.cedexis.com/api/v2/config/accounts.json';

    // stage url - comment out when not testing
    // $postUrl = 'https://portal.dev.cedexis.com/api/v2/config/accounts.json';

    // testing url - for local portal - comment out when not in use
    // $postUrl = 'https://localhost:8443/api/v2/config/accounts.json';
    info('posting to ' . $postUrl);

    $request  = new WP_Http();
    // for language setting def h = ['Accept-Language': this.acceptLanguage]
    $args = array(
    	'method' => 'POST',
    	'body' => json_encode($params),
    	'sslverify' => false,
    	'timeout' => 10, // seconds
    	'headers' => array(
    		'Content-Type' => 'application/json',
    		'Accept' => 'application/json'
    		//,
    	//	'Accept-Language' => 'fr'
    	)
    );
    if (isset($marketoCookieValue)) {
    	$args['cookies'] = array($marketoCookieName => $marketoCookieValue);
    }

    info('args:');
    infoDump($args);

    $response = $request->request($postUrl, $args);
    info('response:');
    infoDump($response);

    //
    // parse the response
    //

    if ($response['response']['code'] !== 204) {
        // fail validation for the entire form
        $validation_result['is_valid'] = false;

        $errors = json_decode($response['body'], true)['errorDetails'];

        // display a message for each invalid field
	    foreach($form['fields'] as &$field) {
	        if (substr($field->cssClass, 0, 7) === $signup) {
	        	// this is a field we are interested in - check if there are related validation errors
	        	info('processing: ' . $field->cssClass);
	        	$paramName = substr($field->cssClass, strlen($signup), strlen($field->cssClass));

	        	foreach($errors as $error) {
	        		if ($error['field'] === $paramName) {
	        			$errorMessage = $error['userMessage'];
	        			info('registering error on [' . $paramName . ']: [' . $errorMessage . ']');
						$field->failed_validation = true;
						$field->validation_message = $errorMessage;
	        		}
	        	}
	        }
	    }
    }


    // assign our modified $form object back to the validation result
    $validation_result['form'] = $form;
    return $validation_result;
}

/*************
Language specific css class
**************/
add_filter('body_class', 'wpml_body_class278594');
function wpml_body_class278594($classes) {
    if(defined('ICL_LANGUAGE_CODE'))
    $classes[] = 'lang-' . ICL_LANGUAGE_CODE;
    return $classes;
}