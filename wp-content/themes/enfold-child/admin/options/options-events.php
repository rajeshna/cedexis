
<?php
/***********************************************
*
*	CEDEXIS EVENTS OPTIONS
*   This file will manage the options available
*   for each event.
*
************************************************/
function cedexis_events_metaboxes() {

	$prefix = '_cedexis_';

	$cedexis_event = new_cmb2_box( array(
		'id'           => $prefix . 'cedexis_event_options',
		'title'        => __( 'Event Details', 'cmb2' ),
		'object_types' => array( 'events', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$cedexis_event->add_field( array(
		'name' => __( 'Event Information', 'cmb2' ),
		'desc' => __( 'Enter the information specific to this event.', 'cmb2' ),
		'id'   => $prefix . 'events_section_title',
		'type' => 'title',
	) );

	$cedexis_event->add_field( array(
		'name' => __( 'Event Date', 'cmb2' ),
		'desc' => __( 'Enter the date that this event will take place.', 'cmb2' ),
		'id'   => $prefix . 'event_date',
		// 'date_format' => 'F j, Y',
		// 'date_format' => 'Ymd',
		'type' => 'text_date',
	) );

	$cedexis_event->add_field( array(
		'name' => __( 'Event URL', 'cmb2' ),
		'desc' => __( 'Enter a URL where this event will link to.', 'cmb2' ),
		'id'   => $prefix . 'event_url',
		'type' => 'text_url',
	) );

	$cedexis_event->add_field( array(
		'name' => __( 'Event Location', 'cmb2' ),
		'desc' => __( 'Enter the location (city) of this event.', 'cmb2' ),
		'id'   => $prefix . 'event_location',
		'type' => 'text',
	) );

	$cedexis_event->add_field( array(
		'name' => __( 'Event Details', 'cmb2' ),
		'desc' => __( 'Add any additional details to this event. i.e. "speaking" or "exhibiting".', 'cmb2' ),
		'id'   => $prefix . 'event_details',
		'type' => 'text',
	) );

}
add_action( 'cmb2_init', 'cedexis_events_metaboxes' );