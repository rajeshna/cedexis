<?php
/****************************************
*
*   EVENTS PAGE AJAX
*
****************************************/

function cedexis_events_ajax(){
  
  if( is_page_template( 'template-events.php' ) ) {

    wp_register_script( 'cedexis-events-ajax', THEME_URL . '/assets/js/events-ajax.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'cedexis-events-ajax' );
    wp_localize_script( 'cedexis-events-ajax', 'headJS', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'templateurl' => get_stylesheet_directory_uri(), 'posts_per_page' => get_option('posts_per_page') ) );
  
  }
  
}
add_action( 'wp_enqueue_scripts', 'cedexis_events_ajax', 90);

add_action( 'wp_ajax_load_more', 'load_more_func' ); // when logged in
add_action( 'wp_ajax_nopriv_load_more', 'load_more_func' );//when logged out

function load_more_func() {

  //verifying nonce here
    if ( !wp_verify_nonce( $_REQUEST['nonce'], 'load_events' ) ) {
      exit('No naughty business please');
    }

  $offset = isset($_REQUEST['offset'])?intval($_REQUEST['offset']):0;
  if( isset( $_REQUEST['taxonomy_year'] ) ) { $year = $_REQUEST['taxonomy_year']; } else { $year = date('Y'); }
     
  ob_start();

         $args = array(
          'post_type' => 'events',
          'posts_per_page' => -1,
          'tax_query' => array(
            array(
              'taxonomy' => 'event_year',
              'field'    => 'name',
              'terms'    => $year,
            ),
          ),
          'meta_query' => array(
	          array(
                    'key' => '_cedexis_event_date',
                    'compare' => 'EXISTS'
                )
          )
        );
        
		$query = new WP_Query($args);
	$press = $query->get_posts();
		

    function fix_events(&$events, $current_year){
	    foreach($events as $event_index => &$event) {
	        if( false === strpos($event->_cedexis_event_date, '/') ) {
	            $str = $event->_cedexis_event_date . " " . $current_year;
	            $date = DateTime::createFromFormat('M d Y',$str);
	            if( $date !== false ) {
		            $output = $date->format('m/d/y');
					$event->_cedexis_event_date = $output;
	            }
	            
	        } else {
	            $event->_cedexis_event_date = date('m/d/y', strtotime($event->_cedexis_event_date));
	        }
	    }
	   
	}
	
	fix_events($press, $year);
	
	usort($press, function($a, $b)
	{
	    return date($a->_cedexis_event_date) < date($b->_cedexis_event_date);
	});
  
  
  if( !empty($press) ) { 

          $result['have_posts'] = true; //set result array item "have_posts" to true
          $date_headings =array();
          foreach($press as $post) { 

            //Post Meta Items
            $event_date = get_post_meta( $post->ID, '_cedexis_event_date', true ); 
            $event_url = get_post_meta( $post->ID, '_cedexis_event_url', true ); 
            $event_location = get_post_meta( $post->ID, '_cedexis_event_location', true ); 
            $event_details = get_post_meta( $post->ID, '_cedexis_event_details', true ); 

            //Conversions
            $date_convert = strtotime( $event_date );
            $date_heading = date('F Y', $date_convert);
            $date_small = date('M j', $date_convert );

          ?>

          <?php 

            if( !in_array($date_heading, $date_headings ) ) { 

              $date_headings[] = $date_heading;
          ?>
            </ul>
            <h3><?php echo $date_heading; ?></h3>
            <ul class="press-calendar events-calendar">
              
          <?php } ?>

            <li>
              <p><span class="event-dates"><strong class="event-start-date"><?php echo $date_small; ?></strong></span><a href="<?php echo $event_url; ?>"><?php echo $post->post_title; ?></a><em><?php echo $event_location; ?></em><span class="status"><?php echo $event_details; ?></span></p>
            </li>

            <?php };
        $result['html'] = ob_get_clean(); // put alloutput data into "html" item
  } else {
      //no posts found
      $result['have_posts'] = false; // return that there is no posts found
  } 
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $result = json_encode($result); // encode result array into json feed
            echo $result; // by echo we return JSON feed on POST request sent via AJAX
        }
        else { 
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
  die();
}



/****************************************
*
*   PRESS & NEWS PAGE AJAX
*
****************************************/

function cedexis_press_ajax(){
  
  if( is_page_template( 'template-press.php' ) ) {

    wp_register_script( 'cedexis-press-ajax', THEME_URL . '/assets/js/press-ajax.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'cedexis-press-ajax' );
    wp_localize_script( 'cedexis-press-ajax', 'headJS', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'templateurl' => get_stylesheet_directory_uri(), 'posts_per_page' => get_option('posts_per_page') ) );
  
  }
  
}
add_action( 'wp_enqueue_scripts', 'cedexis_press_ajax', 90);

add_action( 'wp_ajax_load_press_news', 'load_press_news_func' ); // when logged in
add_action( 'wp_ajax_nopriv_load_press_news', 'load_press_news_func' );//when logged out

function load_press_news_func() {

  //verifying nonce here
    if ( !wp_verify_nonce( $_REQUEST['nonce'], 'load_press_posts' ) ) {
      exit('No naughty business please');
    }

  $offset = isset($_REQUEST['offset'])?intval($_REQUEST['offset']):0;
  if( isset( $_REQUEST['press_taxonomy_year'] ) ) { $press_taxonomy_year = $_REQUEST['press_taxonomy_year']; } else { $press_taxonomy_year = date('Y'); }
     
  ob_start();

        $args = array(
          'post_type' => 'news',
          'posts_per_page' => -1,
          'tax_query' => array(
            array(
              'taxonomy' => 'press_year',
              'field'    => 'name',
              'terms'    => $press_taxonomy_year,
            ),
          ),
          'meta_query' => array(
	          array(
                    'key' => '_cedexis_press_date',
                    'compare' => 'EXISTS'
                )
          )
        );
        
		$query = new WP_Query($args);
	$press = $query->get_posts();
		

    function fix_events(&$events, $current_year){
	    foreach($events as $event_index => &$event) {
	        if( false === strpos($event->_cedexis_press_date, '/') ) {
	            $str = $event->_cedexis_press_date . " " . $current_year;
	            $date = DateTime::createFromFormat('M d Y',$str);
	            if( $date !== false ) {
		            $output = $date->format('m/d/y');
					$event->_cedexis_press_date = $output;
	            }
	            
	        } else {
	            $event->_cedexis_press_date = date('m/d/y', strtotime($event->_cedexis_press_date));
	        }
	    }
	   
	}
	
	fix_events($press, $press_taxonomy_year);
	
	usort($press, function($a, $b)
	{
	    return date($a->_cedexis_press_date) < date($b->_cedexis_press_date);
	});
  
  

	if( !empty($press) ) { 

          $result['have_posts'] = true; //set result array item "have_posts" to true
          $press_date_headings = array();
          
          foreach($press as $post) { 

	          //Post Meta Items
	          $press_date = get_post_meta( $post->ID, '_cedexis_press_date', true ); 
	          $press_url = get_post_meta( $post->ID, '_cedexis_press_url', true ); 
	          $press_source = get_post_meta( $post->ID, '_cedexis_press_source', true ); 
	          $press_translate = get_post_meta( $post->ID, '_cedexis_press_translation', true ); 
	
	          //Conversions
	          $press_date_convert = strtotime( $press_date );
	          $press_date_heading = date('F Y', $press_date_convert);
	          $press_date_small = date('M j', $press_date_convert );
	 
	
		      if( !in_array($press_date_heading, $press_date_headings ) ) { 
		
		      	$press_date_headings[] = $press_date_heading;
		      ?>
		        </ul>
		        <h3><?php echo $press_date_heading; ?></h3>
		        <ul class="press-calendar events-calendar">
		      <?php } ?>
	
	        <li data-date="<?php echo $event->_cedexis_press_date ?>">
	          <p><span class="event-dates"><strong class="event-start-date"><?php echo $press_date_small; ?></strong></span><a target="_blank" href="<?php echo $press_url; ?>">"<?php echo $post->post_title; ?>"</a><?php if ( !empty ($press_translate) && $press_translate == 'yes' ) { echo '<a target="_blank" class="translate" href="http://translate.google.com/translate?js=n&amp;sl=auto&amp;tl=destination_language&amp;u='.$press_url.'">(Translate)</a>'; } ?><em><?php echo $press_source; ?></em></p>
	        </li>

        <?php }
        $result['html'] = ob_get_clean(); // put alloutput data into "html" item
  } else {
      //no posts found
      $result['have_posts'] = false; // return that there is no posts found
  } 
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $result = json_encode($result); // encode result array into json feed
            echo $result; // by echo we return JSON feed on POST request sent via AJAX
        }
        else { 
            header("Location: ".$_SERVER["HTTP_REFERER"]);
        }
  die();
}