jQuery(document).ready(function($){

//javasctipt 
var offset = 7;
var count = 0;

$('.years li a').on('click',function(e){
	e.preventDefault();
	var $load_more_btn = $(this);
	var taxonomyYear = $(this).text();
	var post_type = 'news';
	var nonce = $load_more_btn.attr('data-nonce');
	$.ajax({
    	type : "POST",
    	context: this,
     	dataType : "json",
     	url : headJS.ajaxurl,
		data : {action: "load_press_news", offset:offset, nonce:nonce, post_type:post_type, posts_per_page:-1, press_taxonomy_year:taxonomyYear },
     	
     	beforeSend: function(data) {
			
			$load_more_btn.html('Loading...');
     	
     	},
     	
     	success: function(response) {

			if (response['have_posts'] == 1){//if have posts:
			
				$load_more_btn.html( taxonomyYear );
				var $newElems = $(response['html'].replace(/(\r\n|\n|\r)/gm, ''));// here removing extra breaklines and spaces
				
				$('.news').html( $newElems );
				
			} else {
				//end of posts (no posts found)
				$load_more_btn.html('');
				$load_more_btn.html('<span>No News In </span>' + taxonomyYear );
				setTimeout( function(){
					$load_more_btn.html( taxonomyYear );
				}, 2000);
			}
     	},

     	error: function(response) {

     		console.log( 'error: ');
     		console.log( response );
     	
     	}

  	});
});

});
