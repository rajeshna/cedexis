<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;


// Copied from original enfold-child
if ( ! defined( 'THEME_DIR' ) && ! defined( 'THEME_URL' ) ) {
  define( 'THEME_DIR', get_stylesheet_directory() );
  define( 'THEME_URL', get_stylesheet_directory_uri() );
}
require_once( THEME_DIR . '/admin/ajax.php' );


/*****************************************************
*
*        ADD OPTIONS FOR EVENTS & PRESS RELEASES
*
*****************************************************/
require_once( THEME_DIR . '/admin/options.php' );


/*****************************************************
*
*        EVENTS POST TYPE
*
*****************************************************/
if ( ! function_exists('cedexis_events_post_type') ) {


    // Register Custom Post Type
    function cedexis_events_post_type() {


        $labels = array(
            'name'                  => __( 'Events', 'Post Type General Name', 'cedexis' ),
            'singular_name'         => __( 'Event', 'Post Type Singular Name', 'cedexis' ),
            'menu_name'             => __( 'Events', 'cedexis' ),
            'name_admin_bar'        => __( 'New Event', 'cedexis' ),
            'archives'              => __( 'Item Archives', 'cedexis' ),
            'parent_item_colon'     => __( 'Parent Item:', 'cedexis' ),
            'all_items'             => __( 'All Items', 'cedexis' ),
            'add_new_item'          => __( 'Add New Item', 'cedexis' ),
            'add_new'               => __( 'Add New', 'cedexis' ),
            'new_item'              => __( 'New Event', 'cedexis' ),
            'edit_item'             => __( 'Edit Event', 'cedexis' ),
            'update_item'           => __( 'Update Event', 'cedexis' ),
            'view_item'             => __( 'View Events', 'cedexis' ),
            'search_items'          => __( 'Search Events', 'cedexis' ),
            'not_found'             => __( 'Not found', 'cedexis' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'cedexis' ),
            'featured_image'        => __( 'Featured Image', 'cedexis' ),
            'set_featured_image'    => __( 'Set featured image', 'cedexis' ),
            'remove_featured_image' => __( 'Remove featured image', 'cedexis' ),
            'use_featured_image'    => __( 'Use as featured image', 'cedexis' ),
            'insert_into_item'      => __( 'Insert into item', 'cedexis' ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', 'cedexis' ),
            'items_list'            => __( 'Items list', 'cedexis' ),
            'items_list_navigation' => __( 'Items list navigation', 'cedexis' ),
            'filter_items_list'     => __( 'Filter items list', 'cedexis' ),
        );
        $args = array(
            'label'                 => __( 'Event', 'cedexis' ),
            'description'           => __( 'Cedexis Events', 'cedexis' ),
            'labels'                => $labels,
            'supports'              => array( 'title' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-location-alt',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'post',
        );
        register_post_type( 'events', $args );


    }


    add_action( 'init', 'cedexis_events_post_type', 0 );


}


/*****************************************************
*
*        PRESS POST TYPE
*
*****************************************************/
if ( ! function_exists('cedexis_press_post_type') ) {


// Register Custom Post Type
function cedexis_press_post_type() {


        $labels = array(
                'name'                  => __( 'News & Press', 'Post Type General Name', 'cedexis' ),
                'singular_name'         => __( 'News', 'Post Type Singular Name', 'cedexis' ),
                'menu_name'             => __( 'News & Press', 'cedexis' ),
                'name_admin_bar'        => __( 'New News', 'cedexis' ),
                'archives'              => __( 'Item Archives', 'cedexis' ),
                'parent_item_colon'     => __( 'Parent Item:', 'cedexis' ),
                'all_items'             => __( 'All Items', 'cedexis' ),
                'add_new_item'          => __( 'Add New Item', 'cedexis' ),
                'add_new'               => __( 'Add New', 'cedexis' ),
                'new_item'              => __( 'New News', 'cedexis' ),
                'edit_item'             => __( 'Edit News', 'cedexis' ),
                'update_item'           => __( 'Update News', 'cedexis' ),
                'view_item'             => __( 'View Events', 'cedexis' ),
                'search_items'          => __( 'Search Events', 'cedexis' ),
                'not_found'             => __( 'Not found', 'cedexis' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'cedexis' ),
                'featured_image'        => __( 'Featured Image', 'cedexis' ),
                'set_featured_image'    => __( 'Set featured image', 'cedexis' ),
                'remove_featured_image' => __( 'Remove featured image', 'cedexis' ),
                'use_featured_image'    => __( 'Use as featured image', 'cedexis' ),
                'insert_into_item'      => __( 'Insert into item', 'cedexis' ),
                'uploaded_to_this_item' => __( 'Uploaded to this item', 'cedexis' ),
                'items_list'            => __( 'Items list', 'cedexis' ),
                'items_list_navigation' => __( 'Items list navigation', 'cedexis' ),
                'filter_items_list'     => __( 'Filter items list', 'cedexis' ),
        );
        $args = array(
                'label'                 => __( 'News & Press', 'cedexis' ),
                'description'           => __( 'Cedexis News & Press', 'cedexis' ),
                'labels'                => $labels,
                'supports'              => array( 'title' ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'menu_icon'             => 'dashicons-megaphone',
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'post',
        );
        register_post_type( 'news', $args );


}
add_action( 'init', 'cedexis_press_post_type', 0 );


}




/*****************************************************
*
*        ADD EVENT YEAR TAXONOMY
*
*****************************************************/
// Register Custom Taxonomy
function cedexis_event_year() {


        $labels = array(
                'name'                       => _x( 'Years', 'Taxonomy General Name', 'cedexis' ),
                'singular_name'              => _x( 'Year', 'Taxonomy Singular Name', 'cedexis' ),
                'menu_name'                  => __( 'Event Years', 'cedexis' ),
                'all_items'                  => __( 'All Items', 'cedexis' ),
                'parent_item'                => __( 'Parent Item', 'cedexis' ),
                'parent_item_colon'          => __( 'Parent Item:', 'cedexis' ),
                'new_item_name'              => __( 'New Item Name', 'cedexis' ),
                'add_new_item'               => __( 'Add New Item', 'cedexis' ),
                'edit_item'                  => __( 'Edit Item', 'cedexis' ),
                'update_item'                => __( 'Update Item', 'cedexis' ),
                'view_item'                  => __( 'View Item', 'cedexis' ),
                'separate_items_with_commas' => __( 'Separate items with commas', 'cedexis' ),
                'add_or_remove_items'        => __( 'Add or remove items', 'cedexis' ),
                'choose_from_most_used'      => __( 'Choose from the most used', 'cedexis' ),
                'popular_items'              => __( 'Popular Items', 'cedexis' ),
                'search_items'               => __( 'Search Items', 'cedexis' ),
                'not_found'                  => __( 'Not Found', 'cedexis' ),
                'no_terms'                   => __( 'No items', 'cedexis' ),
                'items_list'                 => __( 'Items list', 'cedexis' ),
                'items_list_navigation'      => __( 'Items list navigation', 'cedexis' ),
        );
        $args = array(
                'labels'                     => $labels,
                'hierarchical'               => true,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => false,
        );
        register_taxonomy( 'event_year', array( 'events' ), $args );


}
add_action( 'init', 'cedexis_event_year', 0 );


/*****************************************************
*
*        ADD PRESS YEAR TAXONOMY
*
*****************************************************/
// Register Custom Taxonomy
function cedexis_press_year() {


        $labels = array(
                'name'                       => _x( 'Years', 'Taxonomy General Name', 'cedexis' ),
                'singular_name'              => _x( 'Year', 'Taxonomy Singular Name', 'cedexis' ),
                'menu_name'                  => __( 'Press Years', 'cedexis' ),
                'all_items'                  => __( 'All Items', 'cedexis' ),
                'parent_item'                => __( 'Parent Item', 'cedexis' ),
                'parent_item_colon'          => __( 'Parent Item:', 'cedexis' ),
                'new_item_name'              => __( 'New Item Name', 'cedexis' ),
                'add_new_item'               => __( 'Add New Item', 'cedexis' ),
                'edit_item'                  => __( 'Edit Item', 'cedexis' ),
                'update_item'                => __( 'Update Item', 'cedexis' ),
                'view_item'                  => __( 'View Item', 'cedexis' ),
                'separate_items_with_commas' => __( 'Separate items with commas', 'cedexis' ),
                'add_or_remove_items'        => __( 'Add or remove items', 'cedexis' ),
                'choose_from_most_used'      => __( 'Choose from the most used', 'cedexis' ),
                'popular_items'              => __( 'Popular Items', 'cedexis' ),
                'search_items'               => __( 'Search Items', 'cedexis' ),
                'not_found'                  => __( 'Not Found', 'cedexis' ),
                'no_terms'                   => __( 'No items', 'cedexis' ),
                'items_list'                 => __( 'Items list', 'cedexis' ),
                'items_list_navigation'      => __( 'Items list navigation', 'cedexis' ),
        );
        $args = array(
                'labels'                     => $labels,
                'hierarchical'               => true,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => false,
        );
        register_taxonomy( 'press_year', array( 'news' ), $args );


}
add_action( 'init', 'cedexis_press_year', 0 );


function cedexis_press_widgets_init() {
    register_sidebar( array(


        'name' => __( 'Press Release Page Sidebar', 'cedexis' ),
        'id' => 'press-sidebar',
        'description' => __( 'Widgets in this area will be shown on the press release page on the right hand side.', 'cedexis' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>',


    ) );
}
add_action( 'widgets_init', 'cedexis_press_widgets_init' );


/** Remove dummy import, just in case. */
add_theme_support('avia_disable_dummy_import');


/** Add custom CSS classes */
add_theme_support('avia_template_builder_custom_css');


/**
 * Custom Cedexis Portal Sign Up function.  Registers GravityForms callback
 * function to tie into a custom form and do an HTTP POST to the Cedexis
 * Portal API for account sign up.
 *
 * Full documentation:
 *
 * https://docs.google.com/a/cedexis.com/document/d/11dZU-orQE0_gvUme9Knagb-2E18xL6Coj_U0993uZBs/edit?usp=sharing
 *
 * @authon Jon Chase <jon.chase@gmail.com>
 */
function cedexis_info( $msg ) {
	error_log( $msg );
}

function cedexis_info_dump( $var ) {
	ob_start();
	var_dump($var);
	$result = ob_get_clean();
	cedexis_info($result);
}

add_filter('gform_validation_14', 'signup_validation'); // _1 is the id of the Gravity Form form in WordPress
function signup_validation($validation_result) {
    // dump the contents of the form - comment this out unless you are debugging
    // cedexis_info_dump($validation_result);

    // the css classes of fields we're interested in are prefixed with this string
    $signup = 'signup-';

    //
    // do not attempt further validation is there is already a validation error
    //
    if (!$validation_result['is_valid']) {
        cedexis_info('form is invalid, not attempting further validation');
        return $validation_result;
    }

    //
    // form is valid, so call the Portal API to try to do a sign up
    //
    // get the GravityForms form object from the validation result
    $form = $validation_result['form'];

    // loop through the form fields and collect them into a hashmap
    $params = array();
    foreach($form['fields'] as &$field) {
        // cedexis_info_dump($field);
        if (substr($field->cssClass, 0, 7) !== $signup) {
            // if the field's css class doesn't start with 'signup-', skip it
            cedexis_info('skipping: ' . $field->cssClass);
        } else {
            // this is a field we are interested in - save the param name and value
            cedexis_info('processing: ' . $field->cssClass);
            $paramName = substr($field->cssClass, strlen($signup), strlen($field->cssClass));

            // special case for newsletter checkbox
            if ($paramName === 'newsletter') {
                $checkboxId = 'input_' . str_replace('.', '_', $field->inputs[0]['id']);
                // have to get value from POST, b/c not available via Gravity Form
                $newsletter = $_POST[$checkboxId];

                if (empty($newsletter)) {
                    $newsletter = FALSE;
                    cedexis_info('newsletter = false');
                } else {
                    $newsletter = TRUE;
                    cedexis_info('newsletter = true');
                }
                cedexis_info_dump($_POST);
                $paramValue = $newsletter;
            } else {
                $paramValue = rgpost("input_{$field['id']}");
                cedexis_info('param val is ' . $paramValue);

                // special case for contact type
                if ($paramName === 'contactType') {
                    if ($paramValue === 'business') {
                        $paramName = 'businessContact';
                    } else if ($paramValue == 'billing') {
                        $paramName = 'billingContact';
                    } else { // technical
                        $paramName = 'technicalContact';
                    }
                    $paramValue = true;
                }
            }

            $params[$paramName] = $paramValue;
        }
    }

    // special case to get the Marketo cookie
    $marketoCookieName = '_mkto_trk';
    $marketoCookieValue = NULL;
    if(isset($_COOKIE[$marketoCookieName])) {
        $marketoCookieValue = $_COOKIE[$marketoCookieName];
    }

    cedexis_info('preparing to post params:');
    cedexis_info_dump($params);

    //
    // post the params to the portal API
    //
    // production url
    $postUrl = 'https://api.cedexis.com/api/v2/config/accounts.json';
    // stage url - comment out when not testing
    // $postUrl = 'https://portal.dev.cedexis.com/api/v2/config/accounts.json';
    // testing url - for local portal - comment out when not in use
    // $postUrl = 'https://localhost:8443/api/v2/config/accounts.json';
    cedexis_info('posting to ' . $postUrl);

    $request  = new WP_Http();
    // for language setting def h = ['Accept-Language': this.acceptLanguage]
    $args = array(
        'method' => 'POST',
        'body' => json_encode($params),
        'sslverify' => false,
        'timeout' => 10, // seconds
        'headers' => array(
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
            //,
            //        'Accept-Language' => 'fr'
        )
    );
    if (isset($marketoCookieValue)) {
        $args['cookies'] = array($marketoCookieName => $marketoCookieValue);
    }

    cedexis_info('args:');
    cedexis_info_dump($args);

    $response = $request->request($postUrl, $args);
    cedexis_info('response:');
    cedexis_info_dump($response);

    //
    // parse the response
    //

    if ($response['response']['code'] !== 204) {
        // fail validation for the entire form
        $validation_result['is_valid'] = false;
        $errors = json_decode($response['body'], true)['errorDetails'];

        // display a message for each invalid field
        foreach($form['fields'] as &$field) {
            if (substr($field->cssClass, 0, 7) === $signup) {
                // this is a field we are interested in - check if there are related validation errors
                cedexis_info('processing: ' . $field->cssClass);
                $paramName = substr($field->cssClass, strlen($signup), strlen($field->cssClass));


                foreach($errors as $error) {
                    if ($error['field'] === $paramName) {
                        $errorMessage = $error['userMessage'];
                        cedexis_info('registering error on [' . $paramName . ']: [' . $errorMessage . ']');
                        $field->failed_validation = true;
                        $field->validation_message = $errorMessage;
                    }
                }
            }
        }
    }

    // assign our modified $form object back to the validation result
    $validation_result['form'] = $form;
    return $validation_result;
}


/********************************************************
 ****      Begin 2017 Cedexis Theme Functions        ****
 ********************************************************/


/** Load our styles after loading the Enfold stuff. */
function cedexis_enqueue_styles() {
    $parent_styles = array('avia-custom', 'avia-style'); // The Enfold main style.


    //wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'cedexis-events-style',
                      get_stylesheet_directory_uri() . '/assets/css/events.css',
                      $parent_styles,
                      wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'cedexis_enqueue_styles' );


/** Load all custom JavaScript scripts
 *
 * All of these scripts are imported from the older header.php and
 * footer.php files from the previous Enfold child theme.
 */
function cedexis_enqueue_scripts() {
    // Radar tags. Why are there three?
    wp_enqueue_script('cedexis-radar-1', '//radar.cedexis.com/1/15343/radar.js', array(), false, true);
  


    // Bizible
    wp_enqueue_script('cedexis-bizible', '//cdn.bizible.com/scripts/bizible.js', array(), false, true);


}
add_action( 'wp_enqueue_scripts', 'cedexis_enqueue_scripts' );


/** Insert custom JavaScript code
 *
 * These snippets were extracted from the old header.php and
 * footer.php files from the previous Enfold child theme. Here they
 * are inserted into the footer to make sure they don't slow down page
 * load times too much.
 */
function cedexis_footer_javascript() {
    print<<<EOT
<!-- Olark -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){ 
f[z]=function(){ 
(a.s=a.s||[]).push(arguments)};var a=f[z]._={ 
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){ 
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={ 
0:+new Date};a.P=function(u){ 
a.p[u]=new Date-a.p[0]};function s(){ 
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){ 
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){ 
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){ 
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{ 
b.contentWindow[g].open()}catch(w){ 
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{ 
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){ 
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({ 
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]}); 
/* custom configuration goes here (www.olark.com/documentation) */ 
olark.identify('4025-815-10-6533');/*]]>*/</script><noscript><a href="https://www.olark.com/site/4025-815-10-6533/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript> 


<!-- HotJar -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:251125,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>


<!--  Quantcast Tag -->
<script>
 var ezt = ezt ||[];
 (function(){
   var elem = document.createElement('script');
   elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://pixel") + ".quantserve.com/aquant.js?a=p-Mh3Hzysc0nSP_";
   elem.async = true;
   elem.type = "text/javascript";
   var scpt = document.getElementsByTagName('script')[0];
   scpt.parentNode.insertBefore(elem,scpt);
 }());
 ezt.push({qacct: 'p-Mh3Hzysc0nSP_', orderid: '', revenue: ''});
</script>
<noscript>
  <img src="//pixel.quantserve.com/pixel/p-Mh3Hzysc0nSP_.gif?labels=_fp.event.Default" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
</noscript>
<!-- End Quantcast Tag -->


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W978XR');</script>
<!-- End Google Tag Manager -->
EOT;
}
add_action( 'wp_footer', 'cedexis_footer_javascript' );


/** Add Museo fonts */
function cedexis_add_heading_font($fonts)
{
    $fonts['Museo Sans'] = 'museo-sans';
    return $fonts;
}
add_filter( 'avf_google_heading_font',  'cedexis_add_heading_font');


function cedexis_add_content_font($fonts)
{
    $fonts['Museo Sans'] = 'museo-sans';
    return $fonts;
}
add_filter( 'avf_google_content_font',  'cedexis_add_content_font');


/** Ubermenu search customization */
function remove_avia_search(){
    remove_filter( 'wp_nav_menu_items', 'avia_append_search_nav', 10, 2 );
}
add_action( 'init' , 'remove_avia_search' );


/** Custom taxonomy for controlling things like the footer. */
// Register Custom Taxonomy
function cedexis_custom_taxonomy() {


        $labels = array(
                'name'                       => _x( 'Targets', 'Taxonomy General Name', 'text_domain' ),
                'singular_name'              => _x( 'Target', 'Taxonomy Singular Name', 'text_domain' ),
                'menu_name'                  => __( 'Target Audience', 'text_domain' ),
                'all_items'                  => __( 'All Items', 'text_domain' ),
                'parent_item'                => __( 'Parent Item', 'text_domain' ),
                'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
                'new_item_name'              => __( 'New Item Name', 'text_domain' ),
                'add_new_item'               => __( 'Add New Item', 'text_domain' ),
                'edit_item'                  => __( 'Edit Item', 'text_domain' ),
                'update_item'                => __( 'Update Item', 'text_domain' ),
                'view_item'                  => __( 'View Item', 'text_domain' ),
                'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
                'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
                'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
                'popular_items'              => __( 'Popular Items', 'text_domain' ),
                'search_items'               => __( 'Search Items', 'text_domain' ),
                'not_found'                  => __( 'Not Found', 'text_domain' ),
                'no_terms'                   => __( 'No items', 'text_domain' ),
                'items_list'                 => __( 'Items list', 'text_domain' ),
                'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
        );
        $args = array(
                'labels'                     => $labels,
                'hierarchical'               => false,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => false,
                'rewrite'                    => false,
        );
        register_taxonomy( 'target', array( 'post', 'page' ), $args );


}
add_action( 'init', 'cedexis_custom_taxonomy', 0 );


/** Add our fancy background image to the default styles. */
function cedexis_add_style ($styles) {
    $cedexisOrange = '#e36e28';
    $cedexisTeal   = '#00a99e';
    $cedexisSlate  = '#414e5e';
    $cedexisFont   = 'Museo Sans';
    
        $styles["Cedexis 2017"] = array(        
        'style'                        => "background-color:$cedexisOrange;",
        'default_font'         => $cedexisFont,
        'google_webfont'=> '',
        'color_scheme'        => 'Cedexis 2017',


        // header or logo?
        'colorset-header_color-bg'                                =>'#000000',
        'colorset-header_color-bg2'                                 =>$cedexisOrange,
        'colorset-header_color-primary'                         =>'#ffffff',
        'colorset-header_color-secondary'                 =>$cedexisOrange,
        'colorset-header_color-color'                         =>'#ffffff',
        'colorset-header_color-border'                         =>'#000000',
        'colorset-header_color-img'                                 =>'',
        'colorset-header_color-customimage'                 =>'',
        'colorset-header_color-pos'                          => 'center center',
        'colorset-header_color-repeat'                          => 'repeat',
        'colorset-header_color-attach'                          => 'scroll',
        'colorset-header_color-heading'                 => '#000000',
        'colorset-header_color-meta'                         => '#808080',
                                                                                                                         
        // main                                                                 
        'colorset-main_color-bg'                                 =>'#ffffff',
        'colorset-main_color-bg2'                                 =>'#fcfcfc',
        'colorset-main_color-primary'                          =>$cedexisOrange,
        'colorset-main_color-secondary'                          =>'#ff985c',
        'colorset-main_color-color'                                  =>'#414e5e',
        'colorset-main_color-border'                          =>'#e1e1e1',
        'colorset-main_color-img'                                 =>'',
        'colorset-main_color-customimage'                 =>'',
        'colorset-main_color-pos'                                  => 'center center',
        'colorset-main_color-repeat'                          => 'repeat',
        'colorset-main_color-attach'                          => 'scroll',
        'colorset-main_color-heading'                         => '#414e5e',
        'colorset-main_color-meta'                                 => '#919191',
                                                                                                                         
        // alternate                                                 
        'colorset-alternate_color-bg'                         =>'#fcfcfc',
        'colorset-alternate_color-bg2'                         =>'#ffffff',
        'colorset-alternate_color-primary'                 =>$cedexisTeal,
        'colorset-alternate_color-secondary'         =>'#1fd1c5',
        'colorset-alternate_color-color'                 =>$cedexisSlate,
        'colorset-alternate_color-border'                 =>'#e1e1e1',
        'colorset-alternate_color-img'                         =>'',
        'colorset-alternate_color-customimage'        =>'',
        'colorset-alternate_color-pos'                          => 'center center',
        'colorset-alternate_color-repeat'                  => 'repeat',
        'colorset-alternate_color-attach'                  => 'scroll',
        'colorset-alternate_color-heading'                 => $cedexisSlate,
        'colorset-alternate_color-meta'                 => '#A0A0A0',
                                                                                                                         
                                                                                                                                                                         
        // Footer
        "colorset-footer_color-bg"                                =>"#ffffff",
        "colorset-footer_color-bg2"                                =>"#333333",
        "colorset-footer_color-primary"                        =>$cedexisSlate,
        "colorset-footer_color-secondary"                =>$cedexisOrange,
        "colorset-footer_color-color"                        =>$cedexisSlate,
        "colorset-footer_color-border"                        =>"#444444",
        "colorset-footer_color-img"                                => "",
        "colorset-footer_color-customimage"                =>"",
        "colorset-footer_color-pos"                         => "top center",
        "colorset-footer_color-repeat"                         => "repeat",
        "colorset-footer_color-attach"                         => "scroll",
        'colorset-footer_color-heading'                 => $cedexisSlate,
        'colorset-footer_color-meta'                         => $cedexisOrange,
                                                
        // Socket
        "colorset-socket_color-bg"                                =>$cedexisTeal,
        "colorset-socket_color-bg2"                                =>$cedexisTeal,
        "colorset-socket_color-primary"                        =>"#ffffff",
        "colorset-socket_color-secondary"                =>"#ffffff",
        "colorset-socket_color-color"                        =>"#ffffff",
        "colorset-socket_color-border"                        =>"#ffffff",
        "colorset-socket_color-img"                                =>"",
        "colorset-socket_color-customimage"                =>"",
        "colorset-socket_color-pos"                         => "top center",
        "colorset-socket_color-repeat"                         => "repeat",
        "colorset-socket_color-attach"                         => "scroll",
        'colorset-socket_color-heading'                 => '#ffffff',
        'colorset-socket_color-meta'                         => '#ffffff',
                                                
        //body bg
        'color-body_style'                                                =>'stretched',
        'color-body_color'                                                =>'#333333',
        'color-body_attach'                                                =>'scroll',
        'color-body_repeat'                                                =>'repeat',
        'color-body_pos'                                                =>'center center',
        'color-body_img'                                                =>'',
        'color-body_customimage'                                =>'',
    );


    return $styles;
}
add_filter( 'avf_skin_options',  'cedexis_add_style');