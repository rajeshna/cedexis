<?php
/*
*	Template name: Press & News
*/
?>
<?php get_header(); ?>
<div id="main" class="all_colors events-main">
<?php if( have_posts() ) : while( have_posts() ) : the_post();  ?>

<section id="masthead">
  <div class="container">
    <div class="template-events-title">
      <h3 class="hero_title">News & Press</h3>
<div class="hero_subtext">Questions about what we do or how our data offers a window into the relative performance of hosting providers, delivery networks and cloud platforms worldwide?  Contact us at media@cedexis.com.</div>
    </div>
  </div>
</section>
<section class="events-content">
  <div class="container">
    <div class="template-events content av-content-full alpha units">
      <div class="post-entry post-entry-type-page">
        <div class="entry-content-wrapper clearfix">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<?php endwhile; ?>
<?php endif; ?>

<section id="press-and-news">
  <div class="container">
    <div class="entry-content-wrapper clearfix">
      <div id="events-sidebar" class="flex_column av_one_fifth flex_column_div av-zero-column-padding first  ">
        <ul class="years">
<?php
	/* GET THE YEAR (TAXONOMY)
         * Construct left hand menu containing all years. */
	$year_args = array( 'hide_empty' => false, 'order' => 'DESC' );
	$years = get_terms( 'press_year', $year_args );
	$available_years = array();

foreach ($years as $year) {
    echo '<li><a href="#" data-nonce="' . wp_create_nonce("load_press_posts") . '">' . $year->name . '</a></li>';
    $available_years[] = $year->slug;
}
?>
        </ul><!--years-->
<?php
if(defined(ICL_LANGUAGE_CODE) && ICL_LANGUAGE_CODE == 'fr'){
    echo '<div class="other-releases"><a class="press-releases" href="/blog/category/cedexis-presse-actualites/">Communiqués de Presse</a></div>';
}
else {
    echo '<div class="other-releases"><a class="press-releases" href="/blog/category/cedexis-news-and-press/">Press Releases</a></div>';
}
?>
    </div><!--events-sidebar-->
<?php
/* GET EVENTS */
$current_year = date('Y');

$args = array(
    'post_type' => 'news',
    'posts_per_page' => -1,
    'tax_query' => array(
        array(
            'taxonomy' => 'press_year',
            'field'    => 'name',
            'terms'    => $current_year,
        ),
    )
);

$query = new WP_Query($args);
$press = $query->get_posts();

function fix_events(&$events, $current_year){
    foreach($events as &$event) {
        if( false === strpos($event->_cedexis_press_date, '/') ) {
            //die("ok");
            $str = $event->_cedexis_press_date . " " . $current_year;
            //die($str);
            $date = DateTime::createFromFormat('M d Y',$str);
            $output = $date->format('m/d/y');
            //die($output);
            $event->_cedexis_press_date = $output;
            
        } else {
            $event->_cedexis_press_date = date('m/d/y', strtotime($event->_cedexis_press_date));
        }
    }
    
}

fix_events($press, $current_year);

usort($press, function($a, $b)
{
    return date($a->_cedexis_press_date) < date($b->_cedexis_press_date);
});
?>
<div id="events-news-list" class="flex_column av_three_fifth flex_column_div av-zero-column-padding">
<?php if( !empty($press) ): ?>

 <?php $date_headings = array(); ?>
 <?php foreach($press as $post): ?>

<?php
				//Post Meta Items
				$press_date = get_post_meta( $post->ID, '_cedexis_press_date', true ); 
				$press_url = get_post_meta( $post->ID, '_cedexis_press_url', true ); 
				$press_source = get_post_meta( $post->ID, '_cedexis_press_source', true ); 
				$press_translate = get_post_meta( $post->ID, '_cedexis_press_translation', true ); 
		
				//Conversions
				$date_convert = strtotime( $press_date );
				$date_heading = date('F Y', $date_convert);
				$date_small = date('M j', $date_convert );
				
?>

<?php if( !in_array($date_heading, $date_headings ) ): ?>

<?php $date_headings[] = $date_heading; ?>
</ul>
<h3><?php echo $date_heading; ?></h3>
<ul class="press-calendar events-calendar">
						
  <?php endif; ?>
  <li data-post-id="<?php echo $post->ID ?>">
    <p>
      <span class="event-dates"><strong class="event-start-date"><?php echo $date_small; ?></strong></span><a target="_blank" href="<?php echo $press_url; ?>">"<?php the_title(); ?>"</a><?php if ( !empty ($press_translate) && $press_translate == 'yes' ) { echo '<a target="_blank" class="translate" href="http://translate.google.com/translate?js=n&amp;sl=auto&amp;tl=destination_language&amp;u='.$press_url.'">(Translate)</a>'; } ?><em><?php echo $press_source; ?></em>
    </p>
  </li>
  <?php endforeach; ?>
</ul>
<?php endif; ?>
</div><!--events-news-list-->
<aside id="press-sidebar">
  <?php dynamic_sidebar('press-sidebar'); ?>
</aside>
</div><!--news-->
</div><!--entry-content-wrapper-->
</div><!--container-->
</section>
</div><!--main-->
<?php get_footer(); ?>
