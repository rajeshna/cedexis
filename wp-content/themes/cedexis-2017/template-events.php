<?php
/*
*	Template name: Events
*/
?>
<?php get_header(); ?>
<div id="main" class="all_colors events-main">
<?php if( have_posts() ) : while( have_posts() ) : the_post();  ?>

<section id="masthead">
  <div class="container">
    <div class="template-events-title">
      <h3 class="hero_title">Events</h3>
<div class="hero_subtext">Along with attending a variety of events, we sponsor and exhibit, as well. These events are a great opportunity to meet with members of our team and discuss our products and solutions in depth. Feel free to come see us!</div>
    <h6>&nbsp;</h6>
</div>
  </div>
</section>

<section class="events-content">
  <div>
    <div class="template-events content av-content-full alpha units">
      <div class="post-entry post-entry-type-page">
        <div class="entry-content-wrapper clearfix">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<?php endwhile; ?>
<?php endif; ?>

<section id="press-and-news">
  <div class="container">
    <div class="entry-content-wrapper clearfix">
      <div id="events-sidebar" class="flex_column av_one_fifth flex_column_div av-zero-column-padding first  ">
        <ul class="years">
<?php
/* GET THE YEAR (TAXONOMY)
 * Construct left hand menu containing all years. */
$year_args = array( 'hide_empty' => false, 'order' => 'DESC' );
$years = get_terms( 'event_year', $year_args );
$available_years = array();

foreach ($years as $year) {
    echo '<li><a href="#" data-nonce="' . wp_create_nonce("load_events") . '">' . $year->name . '</a></li>';
    $available_years[] = $year->slug;
}
?>
        </ul><!--years-->
      </div><!--events-sidebar-->

<?php
/* GET EVENTS */
$current_year = date('Y');
	
$args = array(
    'post_type' => 'events',
    'posts_per_page' => -1,
    'tax_query' => array(
        array(
            'taxonomy' => 'event_year',
            'field'    => 'name',
            'terms'    => $current_year,
        ),
    )
);
	
$query = new WP_Query($args);
$press = $query->get_posts();
	
function fix_events(&$events, $current_year){
    foreach($events as &$event) {
        if( false === strpos($event->_cedexis_event_date, '/') ) {
            //die("ok");
            $str = $event->_cedexis_event_date . " " . $current_year;
            //die($str);
            $date = DateTime::createFromFormat('M d Y',$str);
            $output = $date->format('m/d/y');
            //die($output);
            $event->_cedexis_event_date = $output;
            
        } else {
            $event->_cedexis_event_date = date('m/d/y', strtotime($event->_cedexis_event_date));
        }
    }
    
}

fix_events($press, $current_year);

usort($press, function($a, $b)
{
    return date($a->_cedexis_event_date) < date($b->_cedexis_event_date);
});
?>

<?php if( !empty($press) ): ?>
<div id="events-news-list" class="flex_column av_four_fifth flex_column_div av-zero-column-padding">

  <?php $date_headings = array(); ?>
<?php foreach($press as $post): ?>

<?php
//Post Meta Items
$event_date = get_post_meta( $post->ID, '_cedexis_event_date', true ); 
$event_url = get_post_meta( $post->ID, '_cedexis_event_url', true ); 
$event_location = get_post_meta( $post->ID, '_cedexis_event_location', true ); 
$event_details = get_post_meta( $post->ID, '_cedexis_event_details', true ); 

//Conversions
$date_convert = strtotime( $event_date );
$date_heading = date('F Y', $date_convert);
$date_small = date('M j', $date_convert );
?>

<?php if( !in_array($date_heading, $date_headings ) ): ?>

<?php $date_headings[] = $date_heading; ?>
</ul>
<h3><?php echo $date_heading; ?></h3>
<ul class="press-calendar events-calendar">
<?php endif; ?>
  <li>
    <p><span class="event-dates"><strong class="event-start-date"><?php echo $date_small; ?></strong></span><a href="<?php echo $event_url; ?>"><?php the_title(); ?></a><em><?php echo $event_location; ?></em><span class="status"><?php echo $event_details; ?></span></p>
  </li>
  <?php endforeach; ?>
</ul>
<?php endif; ?>
</div><!--news-->
</div><!--entry-content-wrapper-->
</div><!--container-->
</section>
</div><!--main-->
<?php get_footer(); ?>
