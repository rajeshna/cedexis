<?php
/*
*	Template name: Country Report
*/
?>
<?php get_header(); ?>
<?php if( have_posts() ) : while( have_posts() ) : the_post();  ?>
	<section id="masthead">
		<div class="row">
			<div class="content">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</div>
	</section>
<?php endwhile; ?>
<?php endif; ?>

	<section id="report">
		<div class="row">
			<div class="content">
				<script src="http://www.cedexis.com/country-reports-widget.js"></script> 
				<iframe id="country_report_widget" src="http://www.cedexis.com/reports/country_report_widget.html#?country=US" width="615" height="4000"></iframe> 
			</div>
		</div>
	</section>

<?php get_footer(); ?>