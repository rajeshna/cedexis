
<?php
/***********************************************
*
*	CEDEXIS PRESS RELEASE OPTIONS
*   This file will manage the options available
*   for each news & press item.
*
************************************************/
function cedexis_press_release_metaboxes() {

	$prefix = '_cedexis_';

	$cedexis_press_release = new_cmb2_box( array(
		'id'           => $prefix . 'cedexis_press_options',
		'title'        => __( 'Press Release Details', 'cmb2' ),
		'object_types' => array( 'news', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$cedexis_press_release->add_field( array(
		'name' => __( 'Press Release Information', 'cmb2' ),
		'desc' => __( 'Enter the information specific to this press release.', 'cmb2' ),
		'id'   => $prefix . 'press_section_title',
		'type' => 'title',
	) );
	
	$cedexis_press_release->add_field( array(
		'name' => __( 'Press Release Date', 'cmb2' ),
		'desc' => __( 'Enter the date that this press release was published.', 'cmb2' ),
		'id'   => $prefix . 'press_date',
		// 'date_format' => 'Ymd',
		'type' => 'text_date',
	) );

	$cedexis_press_release->add_field( array(
		'name' => __( 'Press URL', 'cmb2' ),
		'desc' => __( 'Enter a URL where this press release will link to.', 'cmb2' ),
		'id'   => $prefix . 'press_url',
		'type' => 'text_url',
	) );

	$cedexis_press_release->add_field( array(
		'name' => __( 'Source', 'cmb2' ),
		'desc' => __( 'Enter the source of this press release.', 'cmb2' ),
		'id'   => $prefix . 'press_source',
		'type' => 'text',
	) );

	$cedexis_press_release->add_field( array(
		'name' => __( 'Translate Link', 'cmb2' ),
		'desc' => __( 'Select if a translation link should apply to this press release', 'cmb2' ),
		'id'   => $prefix . 'press_translation',
		'type'             => 'radio_inline',
		'options'          => array(
			'no'	=> __('No', 'cmb2'),
			'yes'   => __( 'Yes', 'cmb2' ),
		),
	) );

}
add_action( 'cmb2_init', 'cedexis_press_release_metaboxes' );