<div id="banner"> 
<div class='wrap'>
	<?php screen_icon(); ?>
	   <h1> <a href="https://planetzuda.com/checkout-cyber-security-subscription/?level=5"> Buy DDOS protection today</a> to make sure your site is always accessible! </h1>
	<h2>Redirect controls </h2>
	
	<form method='post' name='redirect-editor' >

		<?php echo wp_nonce_field( 'redirect-editor' ); ?>
		
		<p> Security features: Your wp-config is automatically protected from attackers due to our plugin. You don't need to do anything else. More features coming soon.  
		    <br/>
			Redirect features: Simply enter each redirect  in the following format:
            use the relative domain name, like so http://www.example.com/2012/09/new-post/ . 
			 Followed by the absolute URL of  destination to redirect to, separated by a space . Every redirect is on their own line. You can add comments by going # at the beginning and that line will be ignored. </p>

			 <br/> here is how an example of a redirect would look 
		<p><pre><code>/2012/09/old-post/ http://www.example.com/2012/09/new-post/</code></pre></p>
                <br/> 
				<br/>
	
				
		<p><textarea name='redirects' style='width:100%;height:15em;white-space:pre;font-family:Consolas,Monaco,monospace;'><?php print esc_textarea( $redirects ); ?></textarea></p>

		<p><button type='submit' name='function' class='button button-primary' value='redirect-editor-save'>Save redirect</button></p>

	</form>	
	  
</div>