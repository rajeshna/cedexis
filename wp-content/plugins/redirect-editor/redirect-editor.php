<?php
/*
Plugin Name: Redirect Editor
Version: 1.6
Plugin URI: https://planetzuda.com
Description: redirect editor allows you to make redirects for your site with the easiest redirect editor on WordPress.  Go to <a href="options-general.php?page=redirect-editor">Settings &gt; Redirect Editor</a> to simply type in the redirect for the old url and the new url. We want to provide the simplest way to provide redirecting your posts, pages, etc. and this is exactly what the redirect editor provides.
Author: Planet Zuda
Author URI: https://planetzuda.com
LICENSE
Copyright 2012-2016 Justin Watt
 Copyright 2017 Planet Zuda  sales@planetzuda.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

new Redirect_Editor_Plugin();
class Redirect_Editor_Plugin {

	public function __construct() {
		add_action( 'admin_init', array( $this, 'save_data' ) ); // for researchers, we adopted this code and secured the use of admin_init by requiring an admin to be logged in within function save_data. Before Planet Zuda took over, this was not required.
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
		add_action( 'pre_get_posts', array( $this, 'redirect' ) );
		add_action('init', array($this,'security_protection'));
	}
	public function security_protection()
	{
		// at this time we are currently only blocking the wp-config.php from malicious attacks. A lot more will be added in future updates.
		 $url = 'http://' . $_SERVER["HTTP_HOST"] . esc_url($_SERVER["REQUEST_URI"]);
	 if ( substr_count( $url , 'wp-config' ) > 0 && substr( $url, -1 ) != '/' && ! current_user_can('manage_options') )
		die();
	}

	public function add_admin_menu() {
		add_options_page( 'Redirect Editor', 'Redirect Editor', 'manage_options', 'redirect-editor', array( $this, 'admin_page' ) );
	}

	public function admin_page() {
		$redirects = $this->get_setting('redirects_raw');
		require_once( 'form.php' );
	}

	function get_setting( $name, $default = '') {
		$settings = get_option( 'redirect_editor', array() );

		if ( !is_array( $settings ) ) {
			$settings = array();
		}

		if ( array_key_exists( $name, $settings ) ) {
			return $settings[$name];
		} else {
			return $default;
		}
	}

	// transform POSTed string data into array and save
	// format: /2012/09/old-post/ http://www.example.com/2012/09/new-post/
	function save_data() {
		// since this gets called in the admin_init action, we only want it to 

		
		// run if we're actually processing data for the redirect_editor. Researchers we secured this, if you do find a flaw, please let us know.
	
		if(current_user_can('manage_options'))
		{
		if ( !isset( $_POST['function'] ) || $_POST['function'] != 'redirect-editor-save' ) {
			return;
		}

		if ( isset( $_POST['redirects'] ) && check_admin_referer( 'redirect-editor' ) ) {
			$redirects_rawed =  $_POST['redirects'];
                        $redirects_raw = wp_kses($redirects_rawed,$allowed_html,$allowed_protocols);
			# explode textarea on newline
			$redirect_lines = explode( "\n", $redirects_raw );

			$redirects = array();
			foreach ( $redirect_lines as $redirect_line ) {
				# clean up any extraneous spaces
				$redirect_line = preg_replace( '/\s+/', ' ', trim( $redirect_line ) );

				# skip lines that begin with '#' (hash), treat a comments
				if ( substr( $redirect_line, 0, 1 ) == '#' ) {
					continue;
				}

				# explode each line on space (there should only be one:
				# between the path to match and the destination url)
				$redirect_line = explode( " ", $redirect_line );

				# skip lines that aren't made up of exactly 2 strings, separated by a space
				# other than this, we don't do any validation
				if ( count( $redirect_line ) != 2 ) {
					continue;
				}
				$redirects[$redirect_line[0]] = $redirect_line[1];
			}

			$settings = array();
			$settings['redirects_raw'] = $redirects_raw;
			$settings['redirects'] = $redirects;

			update_option( 'redirect_editor', $settings );
		}

		// we can redirect here because save_data() is called in admin_init action
		wp_redirect( admin_url( 'options-general.php?page=redirect-editor' ) );
	}
}
	// it all comes down to this
	function redirect( $query ) {
		if ( $query->is_main_query() &&  ! current_user_can('manage_options') || $query->is_main_query() && current_user_can('manage_options')) {
			$request_url = esc_url($_SERVER["REQUEST_URI"]);
			$redirects = $this->get_setting( 'redirects', array() );

			if ( array_key_exists( $request_url, $redirects ) ) {
				wp_safe_redirect( $redirects[$request_url], 301 );
				exit;
			}
		}
	}
}
