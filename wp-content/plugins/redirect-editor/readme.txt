=== Redirect Editor And Security ===
Contributors: Planet Zuda, justincwatt, weskoop
website link: https://planetzuda.com
security-flaw:security@planetzuda.com
Tags: wp-config, wp security, DDOS, security, cyber security, security plugin, hackers, crime, redirect, redirection, 301, 301 redirect, htaccess, redirect edit, editor, htaccess edit, 
Requires at least: 3.0
Tested up to: 4.9.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Centrally edit and manage .htaccess-style 301 redirects and your site security.  we provide protection against your most important WP file and will be adding in a lot more free protection. We also provide Premium DDOS protection. 

== Description ==

Features, like are redirects made easy are great, but so is security something you desperately need. We are building up our  free security features and will be releasing more in future updates. We also have premium DDOS protection as well, which stops attackers from knocking your site offline.


We currently provide security protection for:
wp-config.php (free)
DDOS protection (premium)
More security protection coming soon

Our redirect feature lets you redirect your posts, pages and more with ease and simplicity to make redirects for your website. Redirect editor is a great replacement to complicated plugins for managing redirects on your website. The redirect editor simply allows you to add a textarea to edit and manage your 301 redirect, one per line. 

Enter each redirect rule in the following format, starting with the 
relative path of the URL to match, followed by the absolute URL of the 
destination to redirect to, separated by a space. Each redirect should 
be on its own line. Blank lines and lines that start with # (hash) are 
ignored and can be used for spacing and comments. 

Below is an example of how to enter redirects in the redirect editor. As you can see each redirect has a new line and in our example there are two redirects.

    /2012/09/old-post/ http://www.example.com/2012/09/new-post/
 /2012/10/new-post/ http://www.example.com/2012/10/newer-post/


After installing, go to Settings > Redirect Editor to configure.

== Installation ==

Extract the zip file, drop the contents in your wp-content/plugins/ directory, and then activate from the Plugins page.

== Frequently Asked Questions ==
= How secure is this plugin? =
This plugin has been adopted by Planet Zuda, a security company that has updated all of the security and   usability  of this plugin passing testing to show it is a secure plugin. Planet Zuda will be responsible for any  and all future updates.

= Does it support wildcard/regular expression matching? =

No, just simple string matching for the time being.

== Screenshots ==

1. Just a textarea for now.

== Changelog ==
= 1.5.1.1 =
* Fixing an error in the redirect process that caused issues for a few sites.
=1.5.1=
Update wasn't part of our planned release schedule, but due to a report by @awestbha we fixed an issue affecting sites with logged in users that didn't allow redirects if you were already logged in.


=1.5.0=
latest WP version along with some changes to the app. Since this is adopted code, we did change wp_redirect to wp_safe_redirect. 
 This is the last 1.4.x update you should expect before 1.5 is released.  
=1.4.3=
Improved code and updated description of plugin. Expect more updates from https://planetzuda.com . 
= 1.4 =
*Update immediately -- security update. 
*Plugin under new ownership by Planet Zuda, so the safety of redirect editor now is really good. 
* This was removed for 10 months due to abandoned by prior owner. He granted us access to take over. Thank you. 
= 1.3 =
* Fix CSS error causing redirect lines to all appear on a single line

= 1.2 =
* Minor fix for possible corrupted redirects options

= 1.1 =
* Prevent redirect plugin from running on every query, in the admin, etc (thanks Wes Koop)

= 1.0 =
* Initial version

== Upgrade Notice ==
=1.4=
Multiple bug fixes and complete security makeover
= 1.3 =
Bug fix

= 1.2 =
Bug fix

= 1.1 =
Performance enchancement

= 1.0 =
Initial version
