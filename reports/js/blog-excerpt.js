$("#blog_excerpt").each(function() {
    var blog_excerpt = $(this);
    var feed_url = "/blog/feed/rss/";
    // See if this is a French page. If so, use the French RSS feed.
    if (window.location.pathname.indexOf("/fr/") == 0) {
        feed_url = "/fr" + feed_url;
    }
    $.ajax({type: "GET",
            url: feed_url,
            success: function(data) {
                var h5 = $("#blog_excerpt h5 a");
                var p = $("#blog_excerpt p:first");

                // find the first item with a title, description and link
                var item = $(data).find("item:has(title):has(description):has(link):first");

                var title = item.find("title");
                var content = item.find("description");
                var link = item.find("link");

                // Make sure we found what we're looking for
                if (title && content && link && h5 && p) {
                    // Update the dom with the new values
                    var strippedTxt = strip(content.text(), 165);
                    // h5.text(title.text());
                    h5.attr("href", link.text()).text(title.text());
                    p.text(strippedTxt)
                }
            }
    });
    function strip(html, length) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        var txt = tmp.textContent || tmp.innerText;
        txt = txt.substring(0, Math.min(length - 3, txt.length));
        return txt + (txt.length >= length - 3 ? "..." : "");
    }
});
