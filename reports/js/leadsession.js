(function($) {
  'use strict';

  var params = {
        campaign: 'Campaign_Type__c',
        channelsource: 'Channel_Source__c',
        platform: 'Platform__c',
        adcampaign: 'Ad_Campaign__c',
        adgroup: 'Ad_Group__c',
        adtype: 'Ad_Type__c',
        adnetwork: 'Ad_Network__c',
        displayplacement: 'Display_Placement__c',
        keyword: 'Keyword__c',
        device: 'Device__c',
        matchtype: 'Match_Type__c',
        adposition: 'Ad_Position__c',
        placecategory: 'Place_Category__c',
        targetid: 'Target_ID__c',
        os: 'Operating_System__c',
        refererdomain: 'Domain_Referrer__c',
        refererurl: 'URL_Referrer__c'
      };

  function load() {
    var match = ('' + document.cookie).match(/lsrc=([^;]+)/)
      , encoded = (match ? match[1] : '').split('|')
      , values = {}
      , i = 0
      , key;

    for (key in params) {
      values[key] = encoded[i ++];
    }

    return values;
  }

  function save(values) {
    var encoded = []
      , key;

    for (key in params) {
      encoded.push(values[key]);
    }

    document.cookie = 'lsrc=' + encoded.join('|') + ';expires=' + new Date(Date.now() + 86400000).toUTCString() + ';domain=.' + window.location.hostname.split('.').slice(1).join('.') + ';path=/';
  }

  $(function() {
    var previous = load()
      , values = {}
      , referrer = '' + document.referrer
      , referrerDomain = referrer.match(/https?:\/\/([^\/$]+)/i)
      , qs = window.location.search
      , key, match, present;

    for (key in params) {
      match = qs.match(new RegExp(key + '=([^&$]+)', 'i'));
      values[key] = match ? match[1] : '';
      present = present || !!match;
    }

    values.os = navigator.platform;
    values.referrerdomain = referrerDomain ? referrerDomain[1] : '';
    values.referrerurl = referrer;

    if (present) {
      save(values);
    }
    else {
      values = previous;
    }

    setTimeout(function() {
      for (key in params) {
        $('input[name="' + params[key] + '"]').val(values[key]);
      }
    }, 1000);
  });
})(jQuery);
