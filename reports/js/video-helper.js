$(function() {
  if (window.jwplayer) {
    window.jwplayer().onReady(function() {
      var maxWidth = this.config.width
        , ratio = this.config.height / this.config.width
        , size = maxWidth
        , player = this;

      $(window).on('resize', onResize);
      onResize();

      function onResize() {
        var bodyWidth = $('body').width()
          , newSize = bodyWidth > maxWidth ? maxWidth : bodyWidth;

        if (newSize !== size) {
          size = newSize;
          player.resize(size, size * ratio);
        }
      }
    });
  }
});
