$(function() {
  'use strict';

  var configuration = {"widget":{"europe":{"providers":[1,197,198,427,459,471,6,479,21141,500,19278,475,473,493,467,487,497,496,19932,65,20645,20641,251,259,19931,19280,242,45,20638,299,18228,221]}}};

  var util = {
    validDate: function (date) {
      var now = new Date();
      var today = Date.UTC(now.getFullYear(),now.getMonth(),now.getDate());
      var firstValidReportDate =  Date.UTC(2013, 11, 5); //reports before this date have no percentile data
      return date.valueOf() < today.valueOf() && date.valueOf() >= firstValidReportDate.valueOf();
    },
    formatNumber: function(n, decimalPlaces) {
      var out, i;

      decimalPlaces = decimalPlaces || 0;

      n = (Math.round(n * Math.pow(10, decimalPlaces)) / Math.pow(10, decimalPlaces)).toString().split('.');
      out = n[0] + (decimalPlaces > 0 ? '.' : '');

      for (i = 0; i < decimalPlaces; i ++) {
        out += (n[1] || '')[i] || '0';
      }

      return out;
    },
    formatAvailability: function(n) {
      return util.formatNumber(n, 2);
    },
    stringTruncator: function(label, width, suffix) {
      suffix = suffix || '...';

      return function(d) {
        var string = d[label]
          , i;

        d3.select(this).text(string);
        for (i = d[label].length; this.getComputedTextLength() > width; i --) {
          string = d[label].substring(0, i) + suffix;
          d3.select(this).text(string);
        }

        if (d[label][i] == ' ') {
          string = d[label].substring(0, i - 1) + suffix;
        }

        return string;
      };
    },
    urlParam: function(name) {
      var results,
          iframe = new RegExp('[\?&]' + "iframe" + '=([^&#]*)').exec(window.location.href);

      if (iframe && name !== 'country') {
          results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.parent.location.href);
      }
      else {
          results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
      }
      if (results) {
        return results[1];
      }
      else{
        results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(document.referrer);
        if (results) {
          return results[1];
        }
        return null;
      }
    }
  };

  function Reports() {

  }

  Reports.prototype.load = function(raw) {
    var reports = this.reports = {}
      , i, ilen, j, jlen, report, type, index, row;

    // Adds ** to multi X optimized providers
    raw.reports.forEach(function(report){
      report.data.forEach(function(row){
        if(row.id == 1 && row.label.indexOf(" **", row.label.length - 3) === -1){
            row.label = "Cedexis " + row.label;
            row.label += " **";
        }
      });
    });

    for (i = 0, ilen = raw.reports.length; i < ilen; i ++) {
      report = reports[raw.reports[i].category] || (reports[raw.reports[i].category] = {index: [], rows: [], additional: {}});

      if (raw.reports[i].additional) {
        for (j = 0, jlen = raw.reports[i].additional.length; j < jlen; j++) {
          report.additional[raw.reports[i].additional[j].label] = raw.reports[i].additional[j].value;
        }
      }

      if ((type = raw.reports[i].type.match(/(percentile_[0-9]+)_(.*)/)) === null) {
        if (raw.reports[i].type == 'origin') {
          type = [null, 'value', raw.reports[i].type];
        }
        else{
          type = [null, 'mean', raw.reports[i].type];
        }
      }

      for (j = 0, jlen = raw.reports[i].data.length; j < jlen; j ++) {
        if (!~(index = report.index.indexOf(raw.reports[i].data[j].label))) {
          index = report.index.length;
          report.index[index] = raw.reports[i].data[j].label;
          report.rows[index] = {label: raw.reports[i].data[j].label, id: raw.reports[i].data[j].id};
        }

        row = report.rows[index][type[2]] || (report.rows[index][type[2]] = {});
        row[type[1]] = raw.reports[i].data[j].value;

        if (raw.reports[i].unit) {
          row.unit = raw.reports[i].unit;
        }
      }
    }

    return this;
  };

  Reports.prototype.getReport = function(category) {
    return this.reports[category];
  };

  function Report(reports, category) {
    if (reports) {
      this.data = reports.getReport(category);
    }

    this.showAvailability = true;
  }

  Report.prototype.filter = function(probe, statistics) {
    statistics = statistics || ['mean', 'percentile_25', 'percentile_50', 'percentile_75', 'percentile_95'];

    return this.data.rows = this.data.rows.filter(function(row) {
      var i, len;

      for (i = 0, len = statistics.length; i < len; i ++) {
        if (!(probe in row && statistics[i] in row[probe])) {
          return false;
        }
      }

      return true;
    }, this);
  };

  Report.prototype.sort = function(probe, statistic) {
    var order = this.getSortOrder();

    statistic = statistic || 'percentile_50';

    return this.data.rows = this.data.rows.sort(function(a, b) {
      return (order == 'asc' ? a[probe][statistic] - b[probe][statistic] : b[probe][statistic] - a[probe][statistic])
        || (order == 'asc' ? a.error_rate.mean - b.error_rate.mean : b.error_rate.mean - a.error_rate.mean);
    });
  };

  Report.prototype.getSortOrder = function() {
    return 'asc';
  };

  Report.prototype.cluster = function(probe, statistic, clusterCount) {
    var points, pointCount, test, clusters, i, j, clusterSize, means, min, temp, index, sum;

    statistic = statistic || 'percentile_50';
    clusterCount = clusterCount || (this.data.rows.length > 3 ? 3 : 1);

    points = this.data.rows.map(function(row) {
      return row[probe][statistic];
    });

    pointCount = points.length;
    means = [];

    for (i = 0; i < clusterCount; i ++) {
      means.push(points[parseInt(pointCount * (i / clusterCount), 10)]);
    }

    test = [];
    do {
      clusters = test;
      test = new Array(clusterCount);
      for (i = 0; i < clusterCount; i ++) {
        test[i] = [];
      }
      for (i = 0; i < pointCount; i ++) {
        min = Infinity;
        for (j = 0; j < clusterCount; j ++) {
          if ((temp = Math.abs(points[i] - means[j])) < min) {
            min = temp;
            index = j;
          }
        }
        test[index].push(i);
      }

      means = new Array(clusterCount);
      for (i = 0; i < clusterCount; i ++) {
        sum = 0;
        clusterSize = test[i].length;
        for (j = 0; j < clusterSize; j ++) {
          sum += points[test[i][j]];
        }
        means[i] = sum / clusterSize;
      }
    } while (clusters.join('-') != test.join('-'));

    return this.clusters = clusters.sort(function(a, b) {
      return a[0] - b[0];
    });
  };

  Report.prototype.getRows = function() {
    return this.clusters.map(function(cluster) {
      return cluster.map(function(index) {
        return this.data.rows[index];
      }, this);
    }, this);
  };

  Report.prototype.getStats = function() {
    var probe = this.getProbe()
      , probeProviders = this.data.rows.slice().sort(function(a, b) {
          return a[probe].percentile_50 - b[probe].percentile_50;
        })
      , errorProviders = this.data.rows.slice().sort(function(a, b) {
          return a.error_rate.mean - b.error_rate.mean;
        })
      , probeUnit = probeProviders[0][probe].unit
      , errorUnit = probeProviders[0].error_rate.unit
      , stats = [];

    stats.push({
      title: countryReportsStrings.report_fastest_med_rtt,
      provider: probeProviders[0].label,
      value: probeProviders[0][probe].percentile_50 + probeUnit
    },
    {
      title: countryReportsStrings.report_slowest_med_rtt,
      provider: probeProviders[probeProviders.length - 1].label,
      value: probeProviders[probeProviders.length - 1][probe].percentile_50 + probeUnit
    });

    if (this.showAvailability) {
      stats.push({
        title: countryReportsStrings.report_lowest_error_rate,
        provider: errorProviders[0].label,
        value: util.formatAvailability(errorProviders[0].error_rate.mean) + errorUnit
      },
      {
        title: countryReportsStrings.report_highest_error_rate,
        provider: errorProviders[errorProviders.length - 1].label,
        value: util.formatAvailability(errorProviders[errorProviders.length - 1].error_rate.mean) + errorUnit
      });
    }

    return stats;
  };

  Report.prototype.getProbe = function() {
    return 'response_time';
  };

  Report.prototype.getUnit = function() {
    return this.data.rows[0][this.getProbe()].unit;
  };

  Report.prototype.getStrings = function() {
    return {
      unit: this.data.rows[0][this.getProbe()].unit,
      chart_title: countryReportsStrings.report_chart_title_rtt
    };
  };

  function DSAReport(reports) {
    Report.call(this, reports, 'dsa');

    this.filter('response_time');
    this.filter('error_rate', ['mean']);
    this.sort('response_time');
  }

  DSAReport.hasData = function(reports) {
    return !!(reports.getReport('dsa') && reports.getReport('dsa').rows);
  };

  DSAReport.prototype = new Report();
  DSAReport.prototype.constructor = DSAReport;

  DSAReport.prototype.getStats = function() {
    var stats = Report.prototype.getStats.call(this);
    stats[0].title = countryReportsStrings.report_fastest_med_plt;
    stats[1].title = countryReportsStrings.report_slowest_med_plt;
    return stats;
  };

  DSAReport.prototype.getStrings = function() {
    var strings = Report.prototype.getStrings.call(this);
    strings.chart_title = countryReportsStrings.report_chart_title_plt;
    return strings;
  };

  DSAReport.prototype.getRows = function() {
    var grouped_rows = {};
    var result = [];

    this.data.rows.forEach(function(row) {
      var origin = row.origin ? row.origin.value : null;
      if (!grouped_rows[origin]) {
        grouped_rows[origin] = [];
      }
      grouped_rows[origin].push(row);
    });

    // converts the object to a two dimensional array
    for(var group in grouped_rows) {
      result.push(grouped_rows[group]);
    }
    return result;
  };

  //used to draw the titles in the charts
  DSAReport.prototype.getTitles = function() {
    //returns an array with all the origins
    var titles = [];

    this.data.rows.forEach(function(row) {
      if (row.origin && !~titles.indexOf(row.origin.value)) {
        titles.push(row.origin.value);
      }
    });

    return titles;
  };

  function ISPReport(reports) {
    Report.call(this);
    this.data = $.extend(true, {}, reports.getReport('isp'));

    this.filter('unique_sessions', ['mean']);
    this.filter('page_load', ['percentile_50']);
    this.sort('page_load');
    this.cluster('page_load');

    this.data.rows.forEach(function(row) {
      row.page_load.percentile_50 = util.formatNumber(row.page_load.percentile_50, 1);
    });
  }

  ISPReport.hasData = function(reports) {
    return !!(reports.getReport('isp') && reports.getReport('isp').rows);
  };

  ISPReport.prototype = new Report();
  ISPReport.prototype.constructor = ISPReport;

  ISPReport.prototype.getProbe = function() {
    return 'page_load';
  };

  ISPReport.prototype.getStats = function() {
    var stats = Report.prototype.getStats.call(this);
    stats[0].title = countryReportsStrings.report_fastest_med_plt;
    stats[1].title = countryReportsStrings.report_slowest_med_plt;
    return stats;
  };

  ISPReport.prototype.getStrings = function() {
    var strings = Report.prototype.getStrings.call(this);
    strings.chart_title = countryReportsStrings.report_chart_title_plt;
    return strings;
  };

  function CloudReport(reports) {
    Report.call(this, reports, 'cloud');

    this.filter('response_time');
    this.filter('error_rate', ['mean']);
    this.sort('response_time');
    this.cluster('response_time');
  }

  CloudReport.hasData = function(reports) {
    return !!(reports.getReport('cloud') && reports.getReport('cloud').rows);
  };

  CloudReport.prototype = new Report();
  CloudReport.prototype.constructor = CloudReport;

  function CDNRTTReport(reports) {
    Report.call(this, reports, 'cdn');

    this.filter('response_time');
    this.filter('error_rate', ['mean']);
    this.sort('response_time');
    this.cluster('response_time');
  }

  CDNRTTReport.hasData = function(reports) {
    return !!(reports.getReport('cdn') && reports.getReport('cdn').rows);
  };

  CDNRTTReport.prototype = new Report();
  CDNRTTReport.prototype.constructor = CDNRTTReport;

  function CDNTputReport(reports) {
    Report.call(this, reports, 'cdn_throughput');

    this.filter('throughput');
    this.filter('error_rate', ['mean']);
    this.sort('throughput');
    this.cluster('throughput');
  }

  CDNTputReport.hasData = function(reports) {
    return !!(reports.getReport('cdn_throughput') && reports.getReport('cdn_throughput').rows);
  };

  CDNTputReport.prototype = new Report();
  CDNTputReport.prototype.constructor = CDNTputReport;

  CDNTputReport.prototype.getProbe = function() {
    return 'throughput';
  };

  CDNTputReport.prototype.getStrings = function() {
    var strings = Report.prototype.getStrings.call(this);
    strings.chart_title = countryReportsStrings.report_chart_title_tput;
    return strings;
  };

  CDNTputReport.prototype.getStats = function() {
    var stats = Report.prototype.getStats.call(this)
      , probe = this.getProbe()
      , probeProviders = this.data.rows.slice().sort(function(a, b) {
          return b[probe].percentile_50 - a[probe].percentile_50;
        })
      , probeUnit = probeProviders[0][probe].unit;

    stats[0] = {
      title: countryReportsStrings.report_highest_med_tput,
      provider: probeProviders[0].label,
      value: probeProviders[0][probe].percentile_50 + probeUnit
    };
    stats[1] = {
      title: countryReportsStrings.report_lowest_med_tput,
      provider: probeProviders[probeProviders.length - 1].label,
      value: probeProviders[probeProviders.length - 1][probe].percentile_50 + probeUnit
    };

    return stats;
  };

  CDNTputReport.prototype.getSortOrder = function() {
    return 'desc';
  };

  function CloudStorageReport(reports) {
    Report.call(this, reports, 'cloud_storage');

    this.filter('response_time');
    this.filter('error_rate', ['mean']);
    this.sort('response_time');
    this.cluster('response_time');
  }

  CloudStorageReport.hasData = function(reports) {
    return !!(reports.getReport('cloud_storage') && reports.getReport('cloud_storage').rows);
  };

  CloudStorageReport.prototype = new Report();
  CloudStorageReport.prototype.constructor = CloudStorageReport;

  function CDNRTTReport(reports) {
    Report.call(this, reports, 'cdn');

    this.filter('response_time');
    this.filter('error_rate', ['mean']);
    this.sort('response_time');
    this.cluster('response_time');
  }

  function SecureObjectDeliveryReport(reports) {
    Report.call(this, reports, 'secure_object_delivery');

    this.filter('response_time');
    this.filter('error_rate', ['mean']);
    this.sort('response_time');
    this.cluster('response_time');
  }

  SecureObjectDeliveryReport.hasData = function(reports) {
    if (reports.getReport('secure_object_delivery') && reports.getReport('secure_object_delivery').rows) {
      var rows = reports.getReport('secure_object_delivery').rows;
      for(var i = 0; i < rows.length; i ++) {
        if (rows[i].response_time) {
          return true;
        }
      }
    }
    return false;
  };

  SecureObjectDeliveryReport.prototype = new Report();
  SecureObjectDeliveryReport.prototype.constructor = SecureObjectDeliveryReport;

  function ManagedDNSReport(reports) {
    Report.call(this, reports, 'managed_dns');

    this.filter('response_time');
    this.filter('error_rate', ['mean']);
    this.sort('response_time');
    this.cluster('response_time');

    this.showAvailability = false;
  }

  ManagedDNSReport.hasData = function(reports) {
    if (reports.getReport('managed_dns')) {
      var rows = reports.getReport('managed_dns').rows;
      for (var i = rows.length-1; i >= 0; i--) {
        if(!rows[i].error_rate){
          rows[i].error_rate = {mean: 0};
        }
      }
    }

    if (reports.getReport('managed_dns') && reports.getReport('managed_dns').rows) {
      var rows = reports.getReport('managed_dns').rows;
      for(var i = 0; i < rows.length; i ++) {
        if (rows[i].response_time) {
          return true;
        }
      }
    }
    return false;
  };

  ManagedDNSReport.prototype = new Report();
  ManagedDNSReport.prototype.constructor = ManagedDNSReport;

  function Chart(model) {
    this.model = model;
  }

  Chart.prototype.labelFormat = function(platformName, globalPosition, clusterPosition) {
    return globalPosition + '. ' + platformName;
  };

  Chart.prototype.draw = function() {
    var data = this.model.getRows()
      , probe = this.model.getProbe()
      , strings = this.model.getStrings()
      , sortOrder = this.model.getSortOrder()
      , showAvailability = this.model.showAvailability
      , rowHeight = 27
      , barRadius = 3
      , rowCount = data.reduce(function(count, group) {return count + group.length;}, 0)
      , p = [45, 0, 30, 0]
      , graphP = [0, 20, 0, showAvailability ? 340 : 270]
      , w = Math.max(550, $('#chart').width())
      , h = (rowCount + data.length - 1) * rowHeight + p[0] + p[2]
      , x = d3.scale.linear().range([0, w - p[1] - p[3] - graphP[1] - graphP[3]])
      , y = d3.scale.ordinal().domain(d3.range(0, rowCount + data.length - 1))
      , bg = d3.scale.ordinal().range(['top-dark', 'top-light', 'light', 'dark'])
      , stack = d3.layout.stack()
      , statNames = ['percentile_25', 'percentile_50', 'percentile_75', 'percentile_95']
      , statLabels = [
          countryReportsStrings.report_chart_legend_25th,
          countryReportsStrings.report_chart_legend_50th,
          countryReportsStrings.report_chart_legend_75th,
          countryReportsStrings.report_chart_legend_95th
        ]
      , popupNames = statNames.concat('mean')
      , popupLabels = [
          countryReportsStrings.report_popup_percentile_25,
          countryReportsStrings.report_popup_percentile_50,
          countryReportsStrings.report_popup_percentile_75,
          countryReportsStrings.report_popup_percentile_95,
          countryReportsStrings.report_popup_mean
        ];

    if (sortOrder == 'desc') {
      statNames.reverse();
      statLabels.reverse();
    }

    var stats = stack(statNames.map(function(stat, i, stats) {
      return data.reduce(function(values, group, j) {
        return values.concat(group.map(function(row, k) {
          return {
            y: row[probe][stats[i]] - (i ? row[probe][stats[i - 1]] : 0),
            x: values.length + j + k
          };
        }));
      }, []);
    }));

    x.domain([0, d3.max(stats[stats.length - 1], function(d) {return d.y0 + d.y;})]);

    $('#chart').empty();

    var svg = d3.select('#chart').append('svg')
      .attr('width', w)
      .attr('height', h);

    // title/legend

    svg.append('g')
      .attr('class', 'title')
      .call(function(g) {
        g.append('text')
            .attr('x', w / 2)
            .attr('y', 15)
            .text(countryReportsStrings.report_top_platforms);

        var titleWidth = g.select('text').node() !== null ? g.select('text').node().getComputedTextLength() + 20 : 98.626953125;

        g.append('line')
            .attr('y1', 10)
            .attr('y2', 10)
            .attr('x2', w)
            .attr('stroke-dasharray', ((w - titleWidth) / 2) + ',' + titleWidth + ',' + ((w - titleWidth) / 2));
      });

    svg.append('g')
      .attr('class', 'legend')
      .attr('transform', 'translate(0, 35)')
      .call(function(g) {
        g.append('text')
          .attr('x', 10)
          .text(countryReportsStrings.report_platform);

        g.append('text')
          .attr('class', 'stat')
          .attr('x', 263)
          .text(countryReportsStrings.report_med + ' ' + '(' + strings.unit + ')');

        if (showAvailability) {
          g.append('text')
            .attr('class', 'stat')
            .attr('x', 310)
            .text(countryReportsStrings.report_avail);
        }

        if( w > 800 ){
          g.append('text')
            .attr('x', graphP[3])
            .text(strings.chart_title);
        }

        g.append('g')
          .attr('transform', 'translate(' + (w - p[1] - p[3] - 160) + ',0)')
          .selectAll('g.button')
            .data(statLabels)
            .enter().append('g')
              .attr('transform', function(d, i) {return 'translate(' + (i * 40) + ',0)';})
              .attr('class', function(d, i) {return 'button stat-' + statNames[i];})
              .call(function(g) {
                g.append('rect')
                  .attr('class', function(d, i) {return 'stat stat-' + statNames[i];})
                  .attr('height', 10)
                  .attr('width', 10)
                  .attr('y', -9)
                  .attr('rx', 2)
                  .attr('ry', 2);

                g.append('text')
                  .attr('x', 12)
                  .text(function(d) {return d;});

                g.append('rect')
                  .attr('class', 'hit-box')
                  .attr('y', -15)
                  .attr('height', 20)
                  .attr('width', 40)
                  .on('click', function(d, i) {
                    d3.selectAll('#chart svg .active').classed('active', false);
                    d3.selectAll('#chart svg .stat-' + statNames[i]).classed('active', true);
                  });
              });
      });

    svg = svg.append('g')
      .attr('transform', 'translate(' + p[3] + ',' + p[0] + ')');

    // backgrounds
    y.rangeRoundBands([0, h - p[0] - p[2]], 0, 0);

    svg.selectAll('rect.top')
      .data([data[0].length])
      .enter().append('rect')
        .attr('class', 'top top-bg')
        .attr('y', y(0))
        .attr('width', w - p[1] - p[3])
        .attr('height', function(d) {return y(d);});

    var stripe = svg.selectAll('g.background')
      .data([data.reduce(function(rows, group, i) {return rows.concat(d3.range(rows.length + i, rows.length + group.length + i));}, [])])
      .enter().append('g')
        .attr('class', 'background');

    stripe.selectAll('rect')
      .data(Object)
      .enter().append('rect')
        .attr('y', function(d) {return y(d);})
        .attr('width', w - p[1] - p[3])
        .attr('height', y.rangeBand())
        .attr('class', function(d, i) {return bg((i % 2) + (d > data[0].length ? 2 : 0));});

    var graph = svg.append('g')
      .attr('class', 'graph')
      .attr('transform', 'translate(' + graphP[3] + ', ' + graphP[0] + ')');

    // grid
    var grid = graph.append('g')
      .attr('class', 'grid')
      .attr('transform', 'translate(0,' + (h - p[0] - p[2]) + ')')
      .call(d3.svg.axis()
        .scale(x)
        .ticks(10)
        .tickSize(-(h - p[0] - p[2] - 4))
        .tickFormat(function(d) {
          return d + strings.unit;
        }));

    grid.append('line')
      .attr('class', 'border')
      .attr('x2', w - p[1] - p[3] - graphP[3]);

    grid.selectAll('text')
      .attr('y', 12);

    grid.selectAll('.tick')
      .filter(function(d, i) {
        return i % 2;
      })
      .classed('minor', true)
      .select('line')
        .attr('y2', 6);

    grid.selectAll('.tick')
      .filter(function(d, i) {
        return i > 0 && i % 2 === 0;
      })
      .append('line')
        .attr('class', 'below')
        .attr('y2', 8);

    svg.selectAll('g.separators')
      .data(data.reduce(function(bounds, group, i) {return bounds.concat((i ? bounds[i - 1] + 1 : 0) + group.length);}, []).slice(0, -1))
      .enter().append('g')
        .attr('class', 'separator')
        .attr('transform', function(d) {return 'translate(0,' + y(d) + ')';})
        .call(function(row) {
          row.append('rect')
            .attr('width', w - p[1] - p[3])
            .attr('height', y.rangeBand());

          row.append('line')
            .attr('y1', y.rangeBand() / 2)
            .attr('y2', y.rangeBand() / 2)
            .attr('x2', w - p[1] - p[3]);
        });

    // stacked bars
    y.rangeRoundBands([1, h - p[0] - p[2]], 0.55, 0.2);

    var stat = graph.selectAll('g.provider')
      .data(stats.slice(0, -1))
      .enter().append('g')
        .attr('class', function(d, i) {return 'stat stat-' + statNames[i];});

    var rect = stat.selectAll('rect')
      .data(Object)
      .enter().append('rect')
        .attr('y', function(d) {return y(d.x);})
        .attr('x', function(d) {return Math.max(5, x(d.y0));})
        .attr('width', function(d) {return Math.max(0, x(d.y) + (x(d.y0) - Math.max(5, x(d.y0))));})
        .attr('height', y.rangeBand())
        .on('mouseenter', showTooltip)
        .on('mouseleave', hideTooltip);

    stat = graph.selectAll('g.provider')
      .data(stats.slice(-1, stats.length))
      .enter().append('g')
        .attr('class', 'stat stat-' + statNames[stats.length - 1]);

    rect = stat.selectAll("path")
      .data(Object)
      .enter().append("path")
        .style('shape-rendering', 'geometricPrecision ')
        .attr('d', function(d) {
          return 'M' + x(d.y0) + ' ' + y(d.x) +
                 'L' + (x(d.y0 + d.y) - barRadius) + ' ' + y(d.x) +
                 'Q' + x(d.y0 + d.y) + ' ' + y(d.x) + ' ' + x(d.y0 + d.y) + ' ' + (y(d.x) + barRadius) +
                 'L' + x(d.y0 + d.y) + ' ' + (y(d.x) + y.rangeBand() - barRadius) +
                 'Q' + x(d.y0 + d.y) + ' ' + (y(d.x) + y.rangeBand()) + ' ' + (x(d.y0 + d.y) - barRadius) + ' ' + (y(d.x) + y.rangeBand()) +
                 'L' + x(d.y0) + ' ' + (y(d.x) + y.rangeBand()) +
                 'z';
        })
        .on('mouseenter', showTooltip)
        .on('mouseleave', hideTooltip);


    // means circles
    var labelFormat = this.labelFormat;

    stats = [data.reduce(function(values, group, i) {
      return values.concat(group.map(function(row, j) {
        return {
          label: labelFormat(row.label, values.length + j + 1,j + 1),
          mean: row[probe].mean,
          median: row[probe].percentile_50,
          availability: util.formatAvailability(100 - row.error_rate.mean) + row.error_rate.unit,
          y: row[probe].mean,
          x: values.length + i + j
        };
      }));
    }, [])];

    graph.selectAll('g.mean')
      .data(stats)
      .enter().append('g')
        .attr('class', 'mean stat-mean')
        .selectAll('circle')
          .data(Object)
          .enter().append('circle')
            .attr('cy', function(d) {return y(d.x) + y.rangeBand() / 2;})
            .attr('cx', function(d) {return x(d.y);})
            .attr('r', 3.5)
            .on('mouseenter', showTooltip)
            .on('mouseleave', hideTooltip);

    function showTooltip(d, stati) {
      var offset = $(this).offset()
        , width = Math.max(0, x(d.y) + (x(d.y0) - Math.max(5, x(d.y0)))) || 8
        , row, n;

      for (n = 0; n < data.length; n ++) {
        if (stati - data[n].length >= 0) {
          stati -= data[n].length;
        }
        else {
          row = data[n][stati];
          break;
        }
      }

      var popup = d3.select('body').append('svg')
        .attr('id', 'popup')
        .attr('class', 'open')
        .style({
          top: (offset.top - 130) + 'px',
          left: ((width / 2) + offset.left - 100) + 'px',
          height: '130px',
          width: '300px'
        });

      var shape = 'M 10 0 L 190 0 Q 200 0 200 10 L 200 110 Q 200 120 190 120 L 110 120 L 100 130 L 90 120 L 10 120 Q 0 120 0 110 L 0 10 Q 0 0 10 0';

      popup.append('path')
        .attr('class', 'wrapper')
        .attr('d', shape);

      popup.append('path')
        .attr('class', 'header')
        .attr('d', 'M 10 0 L 190 0 Q 200 0 200 10 L 200 25 L 0 25 L 0 10 Q 0 0 10 0');

      popup.append('path')
        .attr('class', 'border')
        .attr('d', shape);

      popup.append('text')
        .datum(row)
        .attr('class', 'title')
        .attr('x', 10)
        .attr('y', 17)
        .text(util.stringTruncator('label', 180));

      popup.selectAll('.value')
        .data(popupNames.map(function(stat, i) {
          return {
            label: popupLabels[i] + ': ' + row[probe][stat] + row[probe].unit,
            stat: stat
          };
        })).enter().append('g')
          .attr('class', 'value')
          .attr('transform', function(d, i) {return 'translate(10, ' + (45 + i * 15) + ')';})
          .call(function(g) {
            g.append('rect')
              .attr('class', function(d, i) {return 'stat stat-' + d.stat + (i == stati ? ' active' : '');})
              .attr('height', 10)
              .attr('width', 10)
              .attr('y', -9)
              .attr('rx', 2)
              .attr('ry', 2);

            g.append('text')
              .attr('x', 15)
              .text(function(d) {return d.label;});
          });

      popup
        .style('opacity', 0)
        .transition()
          .duration(200)
          .style('opacity', 1);
    }

    function hideTooltip(d, i) {
      d3.select('#popup.open')
        .classed('open', false)
        .transition()
          .duration(200)
          .style('opacity', 0)
          .each('end', function() {d3.select(this).remove();});
    }

    // labels
    var label = svg.selectAll('g.label')
      .data(stats)
      .enter().append('g')
        .attr('class', 'label');

    label.selectAll('g.row')
      .data(Object)
      .enter().append('g')
        .attr('class', 'row')
        .attr('transform', function(d) {return 'translate(0, ' + (y(d.x) - 3) + ')';})
        .call(function(row) {
          var name = row.append('text')
            .attr('class', 'name')
            .attr('x', 10);

          row.append('text')
            .attr('class', 'stat')
            .attr('x', 240)
            .text(function(d) {return d.median;});

          if (showAvailability) {
            row.append('text')
              .attr('class', 'stat')
              .attr('x', 310)
              .text(function(d) {return d.availability;});
          }

          row.selectAll('text')
            .attr('y', y.rangeBand());
        })
        .call(function(rows) {
          rows.selectAll('text.name')
            .text(util.stringTruncator('label', 200));
        });

    // initial active state
    d3.selectAll('#chart svg .stat-percentile_50').classed('active', true);
  };

  function ISPChart(model) {
    Chart.call(this, model);
  }

  ISPChart.prototype = new Chart();
  ISPChart.prototype.constructor = ISPChart;

  ISPChart.prototype.draw = function() {
    var w = Math.max(550, $('#chart').width())
      , h = 450
      , radius = 200
      , xLegend = radius * 2 + 50
      , yLegend = 0
      , xChart = 5;

    if((w - radius * 2) < 520){
      xLegend = (w - 500) / 2;
      yLegend = radius * 2;
      h += 350;
      xChart = (w - radius * 2 - 10) / 2;
    }

    Chart.prototype.draw.call(this);

    var svg = d3.select('#chart').insert('svg', ':first-child')
      .attr('class', 'isp-marketshare')
      .attr('width', w + 10)
      .attr('height', h + 10)
      .append('g')
        .attr('transform', 'translate(5, 5)');

    var rows = this.model.data.rows;

    var sum = this.model.data.additional.unique_sessions;

    var others = rows.reduce(function(total, row) {
      return total - row.unique_sessions.mean;
    }, sum);

    if (others > 0) {
      rows = rows.concat([{
        label: countryReportsStrings.others,
        unique_sessions: {
          mean: others
        }
      }]);
    }



    var data = rows.map(function(row, i) {
      var percent = (row.unique_sessions.mean / sum) * 100;

      return {
        label: row.label,
        unique_sessions: row.unique_sessions.mean,
        percent: percent,
        percentLabel: util.formatNumber(percent, percent > 1 ? 0 : 1) + '%',
        chartLabel: percent > 2 ? util.formatNumber(percent) + '%' : ''
      };
    }).sort(function(a, b) {
      return b.unique_sessions - a.unique_sessions;
    }).map(function(d, i) {
      d.index = i;
      return d;
    });

    var ispCount = this.model.data.additional.asns;

    var pie = d3.layout.pie()
      .sort(null);

    var arc = d3.svg.arc()
      .innerRadius(radius - 50)
      .outerRadius(radius);

    svg.append('g')
      .attr('class', 'pie')
      .attr('transform', 'translate(' + (xChart + radius) + ',' + radius + ')')
        .call(function(group) {
          group.selectAll('.section')
            .data(pie(data.filter(function(d) {return d.percent > 0.1;}).map(function(d) {return d.unique_sessions;})).map(function(d, i) {d.index = i; return d;}))
            .enter()
              .call(function(group) {
                group.append('path')
                  .attr('class', function(d, i) {return 'section stat-' + i + ' pie-' + i;})
                  .attr('d', arc)
                  .on('mouseenter', onMouseOver)
                  .on('mouseleave', onMouseOut);

                group.append('text')
                  .attr('class', function(d, i) {return 'pie-' + i;})
                  .attr('x', function(d) {return Math.cos((d.startAngle + d.endAngle - Math.PI) / 2) * (radius - 25);})
                  .attr('y', function(d) {return Math.sin((d.startAngle + d.endAngle - Math.PI) / 2) * (radius - 25) + 6;})
                  .text(function(d, i) {return data[i].chartLabel;})
                  .on('mouseenter', onMouseOver)
                  .on('mouseleave', onMouseOut);
              });

          group.append('text')
            .attr('class', 'secondary')
            .attr('y', -30)
            .text(ispCount.toLocaleString() + ' ' + countryReportsStrings.term_networks);

          group.append('text')
            .attr('class', 'primary')
            .attr('y', 20)
            .text(sum.toLocaleString());

          group.append('text')
            .attr('class', 'secondary')
            .attr('y', 45)
            .text(countryReportsStrings.term_sessions);
        });

    var y = d3.scale.ordinal().domain(d3.range(0, 10)).rangeRoundBands([0, 250], 0.1, 0);

    var legend = svg.selectAll('g.pie-legend')
        .data([data.slice(0, 10), data.slice(10, 20)])
        .enter().append('g')
          .attr('class', 'pie-legend')
          .attr('transform', function(d, i) {return 'translate(' + (i * 250 + xLegend) + ', ' + (yLegend + 125) + ')';});

    legend.selectAll('g.row')
      .filter(function(d, i) {return d.percentLabel;})
      .data(Object)
      .enter().append('g')
        .attr('class', function(d) {return 'row row-' + d.index;})
        .attr('transform', function(d, i) {return 'translate(0, ' + y(i) + ')';})
        .call(function(group) {
          group.append('rect')
            .attr('class', function(d) {return 'stat stat-' + d.index;})
            .attr('height', y.rangeBand())
            .attr('width', 35)
            .attr('rx', 5)
            .attr('ry', 5);

          group.append('text')
            .attr('class', 'stat')
            .attr('x', 35 / 2)
            .attr('y', y.rangeBand() - 6)
            .text(function(d) {return d.percentLabel;});

          group.append('text')
            .attr('class', 'name')
            .attr('x', 50)
            .attr('y', y.rangeBand() - 6);
        })
        .call(function(group) {
          group.selectAll('text.name')
            .text(util.stringTruncator('label', 190));
        })
        .on('mouseenter', onMouseOver)
        .on('mouseleave', onMouseOut);

    svg.append('text')
      .attr('class', 'pie-legend-title')
      .attr('x', xLegend)
      .attr('y', yLegend + 35)
      .text(countryReportsStrings.report_isp_marketshare_title);

    svg.append('foreignObject')
      .attr('class', 'pie-legend-body')
      .attr('x', xLegend)
      .attr('y', yLegend + 45)
      .attr('height', 100)
      .attr('width', Math.min(500, w - xLegend - 20))
      .append('xhtml:body')
        .html(countryReportsStrings.report_isp_marketshare_body);

    function onMouseOver(d) {
      svg.selectAll('.row-' + d.index + ' .stat')
        .transition()
          .duration(100)
          .ease('quad')
          .attr('transform', 'translate(5, 0)');

      svg.selectAll('.pie-' + d.index)
        .transition()
          .duration(100)
          .ease('quad')
          .attr('transform', function(d) {
            var a = (d.startAngle + d.endAngle - Math.PI) / 2;

            return 'translate(' + (Math.cos(a) * 5) + ', ' + (Math.sin(a) * 5) + ')';
          });

      svg.select('.secondary')
        .data([data[d.index]])
          .text(function(d) {return d.label;})
          .text(util.stringTruncator('label', 250));

      svg.select('.primary')
        .data([data[d.index]])
          .text(function(d) {return d.unique_sessions.toLocaleString();});
    }

    function onMouseOut(d) {
      svg.selectAll('.row-' + d.index + ' .stat, .pie-' + d.index)
        .transition()
          .duration(100)
          .ease('quad')
          .attr('transform', '');

      svg.select('.secondary')
        .text(ispCount.toLocaleString() + ' ' + countryReportsStrings.term_networks);

      svg.select('.primary')
        .text(sum.toLocaleString());
    }
  };

  function DSAChart(model) {
    Chart.call(this, model);
  }

  DSAChart.prototype = new Chart();
  DSAChart.prototype.constructor = DSAChart;

  DSAChart.prototype.labelFormat = function(platformName, globalPosition, clusterPosition) {
    return clusterPosition + '. ' + platformName;
  };

  DSAChart.prototype.draw = function() {
    Chart.prototype.draw.call(this);

    var w = Math.max(550, $('#chart').width());

    d3.selectAll('g.title, g.separator')
      .data(this.model.getTitles())
      .attr('class','separator title')
      .call(function(g) {
        g.selectAll("line, text").remove();
        g.append('text')
          .attr('x', w / 2)
          .attr('y', 15)
          .text(function(d) {return d;});

        var titleWidth = g.select('text').node() !== null ? g.select('text').node().getComputedTextLength() + 20 : 98.626953125;

        g.append('line')
          .attr('y1', 10)
          .attr('y2', 10)
          .attr('x2', w)
          .attr('stroke-dasharray', ((w - titleWidth) / 2) + ',' + titleWidth + ',' + ((w - titleWidth) / 2));
      });

    d3.select('#chart').append('div')
      .classed('dsa-details', true)
      .call(function(container) {
        container.append('h4').text(countryReportsStrings.report_dsa_title);
        container.append('div').classed('body', true).html(countryReportsStrings.report_dsa_body);
      });
  };

  function CloudChart(model) {
    Chart.call(this, model);
  }

  CloudChart.prototype = new Chart();
  CloudChart.prototype.constructor = CloudChart;

  function CDNRTTChart(model) {
    Chart.call(this, model);
  }

  CDNRTTChart.prototype = new Chart();
  CDNRTTChart.prototype.constructor = CDNRTTChart;

  function CDNTputChart(model) {
    Chart.call(this, model);
  }

  CDNTputChart.prototype = new Chart();
  CDNTputChart.prototype.constructor = CDNTputChart;

  function CloudStorageChart(model) {
    Chart.call(this, model);
  }

  CloudStorageChart.prototype = new Chart();
  CloudStorageChart.prototype.constructor = CloudStorageChart;


  function SecureObjectDeliveryChart(model) {
    Chart.call(this, model);
  }

  SecureObjectDeliveryChart.prototype = new Chart();
  SecureObjectDeliveryChart.prototype.constructor = SecureObjectDeliveryChart;

  function ManagedDNSChart(model) {
    Chart.call(this, model);
  }

  ManagedDNSChart.prototype = new Chart();
  ManagedDNSChart.prototype.constructor = ManagedDNSChart;

  function Search(countries,initialDate, onCountryChange, onDateChange) {
    this.setCountries(countries);
    this.onCountryChange = onCountryChange;
    this.onDateChange = onDateChange;
    this.$input = $('#search-text');
    this.$date = $('#search-date');
    this.$button = $('#search-button');
    this.$menu = $('<ul id="country-reports-search-menu" />');
    this.menuLength = 0;
    this.fitlered = [];
    this.menuHidden = true;
    this.$date.fdatepicker({
          format: 'yyyy-mm-dd',
          onRender: function (date) {
            return util.validDate(date) ? '' : 'disabled';
          }
      })
      .fdatepicker('setUTCDate', initialDate)
      .on('changeDate',$.proxy(this.changeDate, this));

    this.$input
      .focus($.proxy(this.onFocus, this))
      .blur($.proxy(this.onBlur, this))
      .keydown($.proxy(this.onKeyDown, this));

    this.$menu
      .hide()
      .appendTo('body')
      .on('mouseenter', 'li', $.proxy(this.onMenuMouseEnter, this))
      .on('mouseleave', 'li', $.proxy(this.onMenuMouseLeave, this))
      .on('mouseup', 'li', $.proxy(this.onMenuMouseUp, this))
      .mousedown($.proxy(this.onMenuMouseDown, this));

    this.$button.click($.proxy(function() {
      this.$input.val('');
    }, this));
  }

  Search.prototype.setCountries = function(countries) {
    this.countries = countries.sort(function(a, b) {
      return a.name < b.name ? -1 : 1;
    });
  };

  Search.prototype.onFocus = function(e) {
    this.showMenu();
    setTimeout($.proxy(this.$input.select, this.$input));
  };

  Search.prototype.onBlur = function() {
    var current = this.$input.val()
      , i, len;

    for (i = 0, len = this.countries.length; i < len; i ++) {
      if (this.countries[i].name == current) {
        break;
      }
    }
    if (i == len) {
      this.$input.val('');
    }

    this.hideMenu();
  };

  Search.prototype.onKeyDown = function(e) {
    switch (e.which) {
      case 13:
        this.selectActive();
        this.$button.click();
        return e.preventDefault();
      case 40:
        this.highlightNext();
        return e.preventDefault();
      case 38:
        this.highlightPrev();
        return e.preventDefault();
      case 9:
        this.selectActive();
        return;
      default:
        setTimeout($.proxy(this.filter, this));
    }
  };

  Search.prototype.selectActive = function() {
    var country;
    if (this.highlighted) {
      country = this.filtered[this.highlighted - 1].country;
      this.onCountryChange(country);
      this.$input.val(country.name);
      this.hideMenu();
    }
  };

  Search.prototype.changeDate = function(e) {
    this.onDateChange(e.date);
  };

  Search.prototype.highlightNext = function() {
    if (++ this.highlighted > this.menuLength) {
      this.highlighted = 1;
    }
    this.highlight();
  };

  Search.prototype.highlightPrev = function() {
    if (--this.highlighted < 1) {
      this.highlighted = this.menuLength;
    }
    this.highlight();
  };

  Search.prototype.highlight = function() {
    this.$menu.find('li.active').removeClass('active');
    this.$menu.find(':nth-child(' + this.highlighted + ')').addClass('active');
  };

  Search.prototype.filter = function() {
    var filtered = []
      , rx = new RegExp('(.*)(' + this.$input.val().replace(/[^\s\w'\,\.]/g, '') + ')(.*)', 'i')
      , i, len, match;

    for (i = 0, len = this.countries.length; i < len; i ++) {
      if (match = this.countries[i].name.match(rx)) {
        filtered.push({
          country: this.countries[i],
          before: match[1],
          match: match[2],
          after: match[3]
        });
      }
    }
    this.filtered = filtered.slice(0, 10);
    this.menuLength = len = this.filtered.length;

    if (!len && !this.menuHidden) {
      this.hideMenu();
    }
    else if (len && this.menuHidden) {
      this.showMenu();
    }

    this.$menu.html('');

    for (i = 0; i < len; i ++) {
      $('<li />')
        .append($('<span />').addClass('before').text(filtered[i].before))
        .append($('<span />').addClass('match').text(filtered[i].match))
        .append($('<span />').addClass('after').text(filtered[i].after))
        .appendTo(this.$menu);
    }

    this.highlighted = len == 1 ? 1 : 0;
    this.highlight();
  };

  Search.prototype.showMenu = function() {
    var offset = this.$input.offset();

    this.menuHidden = false;
    this.filter();
    this.$menu.css({
      top: offset.top + this.$input.outerHeight() + 'px',
      left: offset.left + 'px',
      width: this.$input.outerWidth() + 'px'
    }).show();
  };

  Search.prototype.hideMenu = function() {
    this.menuHidden = true;
    this.$menu.hide();
  };

  Search.prototype.onMenuMouseEnter = function(e) {
    this.highlighted = $(e.target).prevAll().length + 1;
    this.highlight();
  };

  Search.prototype.onMenuMouseLeave = function() {
    this.highlighted = 0;
    this.highlight();
  };

  Search.prototype.onMenuMouseDown = function(e) {
    $(window).one('mouseup', $.proxy(this.hideMenu, this));
    e.preventDefault();
  };

  Search.prototype.onMenuMouseUp = function(e) {
    this.selectActive();
  };

  function WorldChart(container) {
    var projection = d3.geo.vanDerGrinten()
            .translate([460, 340])
            .rotate([-10, 0]);

    this.container = container;

    var path = d3.geo.path()
        .projection(projection);

    this.svg = d3.select(container).append('svg');


    var group = this.svg.append("g");

    var scale = group.append("g")
        .attr("class", "scale")
        .attr("transform", "translate(325,490)");

    scale.append('text').attr('text-anchor','end').attr('class','min').attr('x', 40);
    scale.append('text').attr('class','max').attr('x', 265);

    var countries = topojson.feature(mapData, mapData.objects.countries).features;

    group.selectAll(".country")
        .data(countries)
      .enter().insert("path")
        .attr("class", "country")
        .attr("d", path)
        .style("fill", 'rgb(221, 221, 221)') ;

    this.resize();
  }

  WorldChart.prototype.resize = function() {
    var width = Math.max(550, $(this.container).parent().width()),
    height = width / 1.8;

    this.svg
      .attr("width", width)
      .attr("height", height);

    this.svg.select("g")
      .attr("transform", "scale(" + (width / 950) +  ")");
    $(this.container).parent().height(height);
  };

  WorldChart.prototype.update = function(provider, min, max) {
    var group = this.svg.select("g");
    var color = d3.scale.linear()
      .domain([min, (max + min)/2 ,max])
      .range(["blue", "#FE9D52","red"]);

    group
      .selectAll(".country")
      .style("fill", function(d){
        return provider[d.id] ? color(provider[d.id].mean) : 'rgb(221, 221, 221)';
        });

    var scale = group.select('g.scale')
      .selectAll('rect')
      .data(d3.range(1,200,1));

    var coef = (max - min) / 200;

    scale.exit().remove();
    scale.enter().append('rect')
      .attr('width', 1)
      .attr('height', 16)
      .attr('y', -9)
      .attr('x', function(d){ return 50 + d})
      .attr('fill', function(d){ return color(min + d * coef)});

    group.select('.scale .max').text(Math.round(max) + 'ms');
    group.select('.scale .min').text(Math.round(min) + 'ms');
  };

  function ComparisonChart(){
    this.worldChart1 = new WorldChart("#world-chart .chart1");
    this.worldChart2 = new WorldChart("#world-chart .chart2");
    this.provider1 = [];
    this.provider2 = [];

    $("#mix-control").on('mousedown', mouseOn);
    $(document.body).on('mouseup', mouseOff);
    $('select.provider1-selector').on('change', $.proxy( this.provider1Changed, this ));
    $('select.provider2-selector').on('change', $.proxy( this.provider2Changed, this ));

    function mouseOn(e) {
      $(document.body).on('mousemove', mouseMove);
      e.preventDefault();
    }

    function mouseOff() {
      $(document.body).off('mousemove', mouseMove);
    }

    function mouseMove(e) {
      if (e.which === 0 || e.buttons === 0) {
        mouseOff();
      }
      else {
        var newWidth = e.clientX - $("#world-chart .chart1").offset().left;
        var percent = (newWidth / $("#world-chart .chart2").width()) * 100 - 1;
        percent = Math.min(Math.max(5,percent),93);
        $("#world-chart .chart1").width(percent + "%");
      }
    }
  }

  ComparisonChart.prototype.provider1Changed = function(){
    var providerId = $('select.provider1-selector').val();
    this.updateProvider1(providerId);
  };

  ComparisonChart.prototype.provider2Changed = function(){
    var providerId = $('select.provider2-selector').val();
    this.updateProvider2(providerId);
  };

  ComparisonChart.prototype.convertData = function(providerData){
    var result = [];
    providerData.forEach(function(c){
      result[c.country] = c;
    });
    return result;
  };

  ComparisonChart.prototype.updateProvider1 = function(providerId, probeType){
      self = this;
      if(probeType != null && probeType != undefined){
        this.probeType = probeType;
      }else{
        probeType = this.probeType;
      }
      $.get('/widget/api/v1/facts/radar?provider=' + providerId + '&probeType=' + probeType, function(data){
      //$.get('/scaffolding/world-data1.json', function(data){
        self.provider1 = self.convertData(data);
        self.updateCharts();
      });
  };

  ComparisonChart.prototype.updateProvider2 = function(providerId, probeType){
      self = this;
      if(probeType != null && probeType != undefined){
        this.probeType = probeType;
      }else{
        probeType = this.probeType;
      }
      $.get('/widget/api/v1/facts/radar?provider=' + providerId + '&probeType=' + probeType, function(data){
      //$.get('/scaffolding/world-data2.json', function(data){
        self.provider2 = self.convertData(data);
        self.updateCharts();
      });
  };



  ComparisonChart.prototype.resize = function(){
    this.worldChart1.resize();
    this.worldChart2.resize();
  };

  ComparisonChart.prototype.updateCharts = function(){
    var all = this.provider1.concat(this.provider2);
    all = all.filter(function(c){ return c});
    var max = d3.max(all, function(c){return c.mean});
    //Upper limit fixed to 500ms only on Country reports widget compare providers CEDEX-12761
    if ($('.content#chart').data() == null && $('.onlyCloud').data() != null) {
        max = 500;
    }
    var min = d3.min(all, function(c){return c.mean});
    this.worldChart1.update(this.provider1, min, max);
    this.worldChart2.update(this.provider2, min, max);
  };

  ComparisonChart.prototype.updateList = function(providers, probeType){
    providers = providers.filter(function(p){
      return p.id && p.id > 1; //1 == multi X optimized
    }).sort(function (a, b) {
      return a.label.localeCompare(b.label);
    });

    if(providers.length == 0){
      $('#comparison-selectors').hide();
      $('#world-chart').hide();
    }else{
        var providerId1, providerId2,
            compareProvidersChart = ($('.content#chart').data() == null && $('.onlyCloud').data() != null);
        providers.forEach(function(p){
            if (compareProvidersChart){
                if (p.id == 289) { //id 289 == AWS EC2 - US West (CA)
                    providerId1 = p.id;
                }
                else if (p.id == 221) { //id 221 == "AWS EC2 - EU Ireland"
                    providerId2 = p.id;
                }
            }
            else {
                if (p.id == 24 || p.id == 221) { //id 24 == edgecast small CDN ; id 221 == "AWS EC2 - EU Ireland"
                    providerId1 = p.id;
                }
                else if (p.id == 20 || p.id == 479) { //id 20 == limelight CDN ; id 479 == "SFR Cloud - Courbevoie *"
                    providerId2 = p.id;
                }
            }
        });
      $('#comparison-selectors').show();
      $('#world-chart').show();
      ['#comparison-selectors .provider1-selector', '#comparison-selectors .provider2-selector'].forEach(function(select){
        var options = d3.select(select)
          .selectAll('option')
          .data(providers);
        options.exit().remove();
        options.enter().append('option');
        options
          .text(function(d){ return d.label})
          .attr('value', function(d){ return d.id});
      });

      this.updateProvider1((providerId1) ? providerId1 : providers[0].id, probeType);
      this.updateProvider2((providerId2) ? providerId2 : providers[1].id, probeType);
      $('#comparison-selectors .provider1-selector').val((providerId1) ? providerId1 : providers[0].id);
      $('#comparison-selectors .provider2-selector').val((providerId2) ? providerId2 : providers[1].id);
      //Refresh the select
      Foundation.libs.forms.refresh_custom_select($('#comparison-selectors .provider1-selector'), true);
      Foundation.libs.forms.refresh_custom_select($('#comparison-selectors .provider2-selector'), true);
    }
  };

  function getJSON(url) {
    var deferred = $.Deferred()
      , retries = 3;

    (function _getJSON() {
      $.ajax({
        dataType: 'json',
        url: url,
        dataFilter: function(data) {
          return data.replace(/^for\(;;\);/, '');
        }
      })
        .done(deferred.resolve.bind(deferred))
        .fail(function() {
          if (-- retries) {
            setTimeout(_getJSON, 500);
          }
          else {
            deferred.reject();
          }
        });
    })();

    return deferred.promise();
  }

  if (!document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1")) {
    // no svg support
    $('#svg-alert').show();
  }
  else {
    $('#country-report-loading').show();

    var countryParam = '';
    var dateParam = '';
    var providersFilter = null;

    if (util.urlParam('country')) {
      countryParam = util.urlParam('country') + '/';
    }

    if (util.urlParam('date') && util.validDate(new Date(util.urlParam('date')))) {
      dateParam = util.urlParam('date');
    }

    if(util.urlParam('site') && configuration.widget && configuration.widget[util.urlParam('site')]) {
      providersFilter = [];
      providersFilter = configuration.widget[util.urlParam('site')].providers;
    }

    $.when(
      //getJSON('/scaffolding/country_reports.json'),
      //getJSON('/scaffolding/countries.json')
      getJSON('/countryreportsapi/countries/' + countryParam +'reports/days?date=' + dateParam),
      getJSON('/countryreportsapi/countries/days?date=' + dateParam)
    ).done(function(data, countries) {
      var currentCountry = data[0].country.isoCode
        , country = currentCountry
        , reports = new Reports().load(data[0])
        , report = ($('option:selected').data() != null) ? $('option:selected').data().id : $('h4.cloud_response_time').data().id
        , date = data[0].date
        , currentDate = date
        , currentChart
        , comparisonChart
        , models = ($('.content#chart').data() == null && $('.onlyCloud').data() != null) ?
          {
            cloud_response_time: CloudReport
          }
          :
          {
            isp: ISPReport,
            dsa: DSAReport,
            cloud_response_time: CloudReport,
            cdn_response_time: CDNRTTReport,
            cdn_throughput: CDNTputReport,
            cloud_storage_response_time: CloudStorageReport,
            secure_object_delivery_response_time: SecureObjectDeliveryReport//,
            //managed_dns: ManagedDNSReport
          }
        , views = ($('.content#chart').data() == null && $('.onlyCloud').data() != null) ?
          {
             cloud_response_time: CloudChart
          }
          :
          {
            isp: ISPChart,
            dsa: DSAChart,
            cloud_response_time: CloudChart,
            cdn_response_time: CDNRTTChart,
            cdn_throughput: CDNTputChart,
            cloud_storage_response_time: CloudStorageChart,
            secure_object_delivery_response_time: SecureObjectDeliveryChart//,
            //managed_dns: ManagedDNSChart
          }
         , iframeWidth= $('country_report').context.activeElement.clientWidth,
           variableMargin;

         //Depending on iframeWidth it will set a different margin to center the text
         switch (true) {
            case (iframeWidth < 580):
                variableMargin = "10%";
                break;
            case (iframeWidth >= 580 && iframeWidth < 640):
                variableMargin = "13%";
                break;
            case (iframeWidth >= 640 && iframeWidth < 720):
                variableMargin ="16%";
                break;
            case (iframeWidth >= 720 && iframeWidth < 800):
               variableMargin ="19%";
               break;
            case (iframeWidth >= 800 && iframeWidth < 850):
                variableMargin ="21%";
                break;
            case (iframeWidth >= 850 && iframeWidth < 900):
                variableMargin ="23%";
                break;
            case (iframeWidth >= 900):
                variableMargin ="25%";
                break;
         }
         $('#comparison-selectors .custom #compare-providers p').css("margin-left",variableMargin);


      if (util.urlParam('report') && util.urlParam('report') in models) {
        report = util.urlParam('report');
      }
      updateUrl();

      if ($("#world-chart").length){
        comparisonChart = new ComparisonChart();
      }

      $('#country-report-loading').hide();
      $('#country-report').show();

      var search = new Search(countries[0], new Date(date), function(value) {
        country = value.isoCode;
      },
      function(value) {
        date = value.toISOString().substr(0,10); //Date in format yyyy-mm-dd
        getJSON('/countryreportsapi/countries/days?date=' + date).done(function(data) {
          search.setCountries(data);
        });
      }
      );

      function updateCountryData() {
        if (currentCountry != country || currentDate != date) {
          $('#loading-alert').hide();
          $('#country-report-loading-new').show();

          //getJSON('/scaffolding/country_reports.json?currentCountryCode=' + country)
          getJSON('/countryreportsapi/countries/' + country + '/reports/days?date=' + date)
            .done(function(data) {
              $('#country-report-loading-new').hide();
              update(data);
              drawReport();
            })
            .fail(function() {
              $('#country-report-loading-new').hide();
              $('#loading-alert').show();
            });
          currentCountry = country;
          currentDate = date;
          updateUrl();
        }
      }

      function hideShare() {
        $('#share-url').hide();
      }

      function copyToClipboard() {
        $('input[name="urltoshare"]').select();
        try {
          document.execCommand('copy');
        } catch (err) {}
      }

      function showShare() {
        var p = $('#share-button').position(),
          x = (p.left - 405) + 'px',
          y = (p.top) + 'px',
          url = 'http://stg.cedexis.com/get-the-data/country-report/';

        if (util.urlParam('shareurl')) {
          url = util.urlParam('shareurl');
        }

        $('#share-url').css({
          "left": x,
          "top": y
        });

        $('input[name="urltoshare"]').val(url + "?report=" + report + "&country=" + country + "&date=" + date);
        $('#share-url').show();
      }

      $('#copytoclip').on('click', copyToClipboard);

      $('#closemodal').on('click', hideShare);

      $('#share-button').on('click', showShare);

      $('#search-button').on('click', updateCountryData);

      // $('.tabs').on('click', 'li', function() {
      //   $('.tabs .active').removeClass('active');
      //   $(this).addClass('active');
      //   report = $(this).data('id');
      //   drawReport();
      //   updateUrl();
      // });

      $('.report-selector').on('change', function() {
        report = ($('option:selected').data('id') !== null) ? $('option:selected').data('id') : $('h4.cloud_response_time').data('id');
        drawReport();
        updateUrl();
      });

      $('.report-selector li.selected').removeClass('selected');
      $('.report-selector li.' + report).addClass('selected');
      $('.report-selector a.current').text($('.report-selector li.selected').text());

      function locationHashChanged() {
        if (util.urlParam('report')) {
          report = util.urlParam('report');
          $('.tabs .active').removeClass('active');
          $('.tabs [data-id="' + report + '"]').addClass('active');
          $('.report-selector li.selected').removeClass('selected');
          $('.report-selector li.' + report).addClass('selected');
          $('.report-selector a.current').text($('.report-selector li.selected').text());
        }
        if (util.urlParam('country')) {
          country = util.urlParam('country');
        }
        if (util.urlParam('date') && util.validDate(new Date(util.urlParam('date')))) {
          date = util.urlParam('date');
          search.$date.fdatepicker('setUTCDate', new Date(date));
        }
        updateCountryData();
        drawReport();

        ga('send', 'event', {
          eventCategory: 'reports',
          eventAction: 'view country',
          eventLabel: country,
          page: window.location.pathname + window.location.hash.slice(1)
        });
      }

      if ("onhashchange" in window) {
        //hashchange event supported
        window.onhashchange = locationHashChanged;
      }

      $('.tabs [data-id="' + report + '"]').addClass('active');

      function updateUrl() {
        location.hash = '?report=' + report + '&country=' + currentCountry + '&date=' + date;
      }

      $(window).resize(function(){
        // This information can used when the page is rendered inside an iframe
        // by the parent window to adjust the size of the element.
        // (The only reason to prepend '00000' in the message is to avoid
        // conflicts with the radar widget if it's being included in the same page)
        window.parent.postMessage('00000' + JSON.stringify({height: $('body').height()}), '*');
        if(currentChart){
            currentChart.draw();
        }
        if(comparisonChart){
          comparisonChart.resize();
        }
      });

      function drawReport() {
        var model = new models[report](reports)
          , chart = new views[report](model)
          , stats = model.getStats();

        var probeType = report == 'cdn_throughput' ? 14 : 0;

        if(comparisonChart){
          //We only have data for cdn's and clouds for now
          if(report.indexOf("cdn") > -1 || report.indexOf("cloud") > -1){
            $('#comparison-selectors').show();
            if(report.indexOf("cdn") > -1) {
              $('#comparison-selectors .custom #compare-providers .text1').hide();
              $('#comparison-selectors .custom #compare-providers .text2').show();
            }
            else {
                $('#comparison-selectors .custom #compare-providers .text1').show();
                $('#comparison-selectors .custom #compare-providers .text2').hide();
            }
            $('#world-chart').show();
            comparisonChart.updateList(model.data.rows, probeType);
          }else{
            $('#comparison-selectors').hide();
            $('#world-chart').hide();
          }
        }
        chart.draw();
        currentChart = chart;

        for (var i = 0; i < 4; i ++) {
          if (stats[i]) {
            $('#stat-title-' + i).show().text(stats[i].title);
            $('#stat-provider-' + i).show().text(stats[i].provider);
            $('#stat-value-' + i).show().text(stats[i].value);
          }
          else {
            $('#stat-title-' + i + ', #stat-provider-' + i + ', #stat-value-' + i).hide();
          }
        }
      }

      function filter(data){
        if(providersFilter){
          data.reports = data.reports.filter(function(report){
            if (report.category == "cloud" && report.type == "response_time") {
                report.data = report.data.filter(function (d) {
                    return d.id == null || providersFilter.indexOf(d.id) > -1;
                });
            }
            return report.data.length > 0;
          });
        }
        return data;
      }

      function update(data) {
        var newReports = new Reports().load(filter(data))
          , candidates = {}
          , valid;
        date = data.date;
        var dateParts = date.split('-')

        for (var name in models) {
          valid = candidates[name] = models[name].hasData(newReports);
          $('.report-selector li.' + name + '').toggle(valid);
        }

        if (!candidates[report]) {
          candidates = Object.keys(candidates);

          if (!candidates.length) {
            $('#loading-alert').show();
            return;
          }
          else {
            report = candidates[0];
            updateUrl();
          }
        }

        reports = newReports;
        $('#date').text(countryReportsStrings.months[dateParts[1] - 1] + ' ' + dateParts[2] + ', ' + dateParts[0]);
        $('#country').text(data.country.name);
      }

      update(data[0]);
      drawReport();
      if(comparisonChart){
        comparisonChart.resize();
      }
    })
    .fail(function() {
      $('#country-report-loading').hide();
      $('#loading-alert').show();
    });
  }
});
